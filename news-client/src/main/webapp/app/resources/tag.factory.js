(function () {
  'use strict';

  angular.module('news').factory('tagFactory', tagFactory);

  tagFactory.$inject = ['$resource'];

  function tagFactory($resource) {
    var service = {};
    var tags = $resource('api/tag/all').query();

    service.getAll = function () {
      return tags;
    };

    //service.loadTags = function () {
    //  tags
    //};

    return service;
  }
})();