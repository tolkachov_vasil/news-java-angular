(function () {

  angular.module('news').factory('authorFactory', authorFactory);

  authorFactory.$inject = ['$resource'];

  function authorFactory($resource) {
    var service = {};
    var authors = $resource('api/author/all').query();
    var selected = null;


    service.select = function (auth) {
      selected = auth;
    };

    service.unSelect = function () {
      selected = {authorName: 'All authors', authorId: 0};
    };

    service.getSelected = function () {
      return selected;
    };

    service.getAll = function () {
      return authors;
    };

    return service;
  }
})();