(function () {

  angular.module('news').factory('commentFactory', commentFactory);

  commentFactory.$inject = ['$resource', '$q'];

  function commentFactory($resource, $q) {

    var service = {};

    service.saveComment = function (newsId, text) {
      var deferred = $q.defer();
      var comment = {
        commentText: text,
        newsId: newsId,
        creationDate: new Date()
      };
      $resource('api/comment').save(comment).$promise
        .then(function (newComment) {
          deferred.resolve(newComment);
        }, function (reason) {
          deferred.reject(reason);
        });
      return deferred.promise;
    };

    return service;
  }
})();