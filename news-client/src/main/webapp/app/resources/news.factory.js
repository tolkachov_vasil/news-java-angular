(function () {

  angular.module('news').factory('newsFactory', newsFactory);

  newsFactory.$inject = ['$resource', '$q'];

  function newsFactory($resource, $q) {

    var service = {};

    service.loadNews = function (id) {
      var deferred = $q.defer();
      $resource('api/news/:id').get({id: id}).$promise
        .then(function (news) {
          deferred.resolve(news);
        }, function (reason) {
          deferred.reject(reason);
        });
      return deferred.promise;
    };

    return service;
  }
})();