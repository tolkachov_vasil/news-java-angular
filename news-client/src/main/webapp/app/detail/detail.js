(function () {
  'use strict';

  angular.module('news')

    .component('newsDetailView', {
      templateUrl: '/app/detail/detail.html',
      bindings: {news: '<', back: '<'},
      controller: ['commentFactory',
        function (commentFactory) {
          var vm = this;
          this.saveComment = function (data, addCommentForm) {
            if (addCommentForm.$valid) {
              commentFactory
                .saveComment(vm.news.news.newsId, data.commentText)
                .then(function (newComment) {
                  data.commentText = '';
                  vm.news.news.comments.push(newComment);
                })
            }
          }
        }]
    })

    .config(['$stateProvider', function ($stateProvider) {
      $stateProvider.state('news.detail', {
        url: 'page/:pageNum/news/:newsId',
        template: '<news-detail-view news="news" back="back"/>',
        resolve: {
          news: ['newsFactory', '$stateParams',
            function (newsFactory, $stateParams) {
              return newsFactory.loadNews($stateParams.newsId);
            }]
        },
        controller: ['$scope', 'news', '$stateParams',
          function ($scope, news, $stateParams) {
            $scope.news = news;
            $scope.back = $stateParams.pageNum;
          }]
      });
    }]);

})();