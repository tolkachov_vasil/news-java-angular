(function () {
  'use strict';

  angular.module('news')

    .component('authorTagFilter', {
      templateUrl: '/app/list/filter/filter.html',
      controller: ['filterFactory',
        function (filterFactory) {
          var vm = this;

          vm.filter = filterFactory.filter;

          vm.resetFilter = function () {
            filterFactory.resetFilter();
          };

          vm.applyFilter = function () {
            filterFactory.applyFilter();
          };

          vm.updateTagsTitle = function () {
            filterFactory.updateTagsTitle();
          };

        }]
    })

    .config(['$httpProvider', function ($httpProvider) {

      $httpProvider.interceptors.push(function (filterFactoryUrl) {
        return {
          request: function (config) {
            var url = config.url;
            if (url.match(/^api\/page\//)) {
              config.url = url + filterFactoryUrl.getFilterUrl();
            }
            return config;
          }
        };
      });
    }]);

})();