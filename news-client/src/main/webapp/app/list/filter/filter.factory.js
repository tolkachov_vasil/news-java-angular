(function () {
  'use strict';

  angular.module('news').factory('filterFactory', filterFactory);
  angular.module('news').factory('filterFactoryUrl', filterFactoryUrl);

  filterFactory.$inject = ['tagFactory', 'authorFactory', '$state', 'filterFactoryUrl'];

  function filterFactory(tagFactory, authorFactory, $state, filterFactoryUrl) {
    var service = {};

    service.filter = {
      tags: tagFactory.getAll(),
      authors: authorFactory.getAll(),
      selectedAuthor: '',
      authorId: 0,
      tagIds: []
    };

    service.resetFilter = resetFilter;
    service.updateTagsTitle = updateTagsTitle;
    service.applyFilter = applyFilter;

    function resetFilter() {
      service.filter.selectedAuthor = '';
      service.filter.tags.forEach(function (tag) {
        delete(tag.enabled);
      });
      service.filter.tagIds = [];
      applyFilter();
    }

    function updateTagsTitle() {
      service.filter.tagIds = [];
      service.filter.tags.forEach(function (tag) {
        if (tag.enabled) {
          service.filter.tagIds.push(tag.tagId);
        }
      });
    }

    function applyFilter() {

      updateTagsTitle();

      service.filter.authorId = 0;
      service.filter.authors.forEach(function (author) {
        if (author.authorName === service.filter.selectedAuthor) {
          service.filter.authorId = author.authorId;
        }
      });

      updateFilterUrl();
      $state.go('news.list', {pageNum: 1}, {reload: true});
    }

    function updateFilterUrl() {
      if (service.filter.tagIds.length !== 0 && service.filter.authorId === 0) {
        filterFactoryUrl.setFilterUrl('/tags/' + service.filter.tagIds.join()); // tags only
      } else if (service.filter.tagIds.length === 0 && service.filter.authorId !== 0) {
        filterFactoryUrl.setFilterUrl('/author/' + service.filter.authorId); // author only
      } else if (service.filter.tagIds.length !== 0 && service.filter.authorId !== 0) {
        filterFactoryUrl.setFilterUrl('/author/' + service.filter.authorId + '/tags/' + service.filter.tagIds.join()); // tags & author
      } else
        filterFactoryUrl.setFilterUrl(''); // no filter
    }

    return service;
  }


  function filterFactoryUrl() {
    var filterUrl = '';
    var service = {};

    service.getFilterUrl = function () {
      return filterUrl;
    };

    service.setFilterUrl = function (url) {
      filterUrl = url;
    };

    return service;
  }

})();