(function () {
  'use strict';

  angular.module('news')

    .component('pageView', {
      templateUrl: '/app/list/page/page.html',
      bindings: {page: '<'}
    });

})();