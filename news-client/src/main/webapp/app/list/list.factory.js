(function () {
  'use strict';
  angular.module('news').factory('newsListFactory', newsListFactory);

  newsListFactory.$inject = ['$resource', '$q'];

  function newsListFactory($resource, $q) {
    var service = {};

    service.loadPage = function (pageNum) {
      var deferred = $q.defer();
      $resource('api/page/:pageNum').get({pageNum: pageNum}).$promise
        .then(function (page) {
          deferred.resolve(page);
        }, function (reason) {
          deferred.reject(reason);
        });
      return deferred.promise;
    };

    return service;
  }

})();
