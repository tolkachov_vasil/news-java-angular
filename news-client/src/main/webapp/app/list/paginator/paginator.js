(function () {
  'use strict';

  angular.module('news')

    .component('newsPaginator', {
      templateUrl: '/app/list/paginator/paginator.html',
      bindings: {page: '<'},
      controller: function () {
        var vm = this;

        vm.paginate = [];
        if (vm.page.totalPages > 1) {
          for (var i = 1; i <= vm.page.totalPages; i++) {
            vm.paginate.push({
              num: i,
              state: ''
            });
          }
          vm.paginate[vm.page.number - 1].state = 'active';
        }
      }
    });

})();


