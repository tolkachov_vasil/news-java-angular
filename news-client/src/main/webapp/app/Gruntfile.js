module.exports = function (grunt) {

  grunt.initConfig({
    nggettext_extract: {
      pot: {
        files: {
          'po/template.pot': '**/*.html'
        }
      }
    },
    nggettext_compile: {
      all: {
        options: {
          module: 'news'
        },
        files: {
          'i18n/translations.js': ['po/*.po']
        }
      }
    },
    watch: {
      options: {
        livereload: true
      },
      css: {
        files: ['**/*.html']
        //tasks: ['livereload']
      }
    }
  });

  grunt.loadNpmTasks('grunt-angular-gettext');
  grunt.loadNpmTasks('grunt-contrib-watch');

  //grunt.registerTask('default', ['nggettext_extract']);
  grunt.event.on('watch', function (action, filepath, target) {
    grunt.log.writeln(target + ': ' + filepath + ' has ' + action);
  });

};