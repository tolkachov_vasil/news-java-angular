(function () {
  'use strict';

  var config = ['$stateProvider', '$urlRouterProvider', '$locationProvider',
    function ($stateProvider, $urlRouterProvider, $locationProvider) {
      $urlRouterProvider.otherwise('/page/1');
      $locationProvider.hashPrefix('!');
      $stateProvider.state('news', {
        url: '/',
        abstract: true,
        template: '<main-view/>'
      });
    }
  ];


  var init = ['$rootScope', '$state', '$stateParams',
    function ($rootScope, $state, $stateParams) {
      $rootScope.$state = $state;
      $rootScope.$stateParams = $stateParams;
      $rootScope.$on('$stateChangeStart', function () {
        angular.element(document.getElementsByTagName('body')).attr('data-status', 'loading');
      });


      $rootScope.$on('$stateChangeSuccess', function () {
        angular.element(document.getElementsByTagName('body')).attr('data-status', 'ready');
      });
    }
  ];

  angular.module('news', ['ngResource', 'ui.router', 'once', 'gettext'])
    .config(config)
    .run(init);

})();
