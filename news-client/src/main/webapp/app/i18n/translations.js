angular.module('news').run(['gettextCatalog', function (gettextCatalog) {
  /* jshint -W100 */
  gettextCatalog.setStrings('ru', {
    "(by": "(от",
    "All authors": "Все авторы",
    "BACK": "НАЗАД",
    "Comments(": "Комментарии(",
    "Filter": "Фильтр",
    "NEXT": "ДАЛЕЕ",
    "News Portal": "Новостной портал",
    "News not found for this criteria": "По данному критерию новостей не найдено",
    "PREVIOUS": "РАНЕЕ",
    "Reset": "Сброс",
    "Select tags": "Выбрать теги",
    "Send Comment": "Оставить комментарий",
    "Tags selected:": "Выбрано тегов: ",
    "View": "Подробнее"
  });
  /* jshint +W100 */
}]);