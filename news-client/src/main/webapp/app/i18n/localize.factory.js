(function () {
  'use strict';

  angular.module('news')
    .factory('localize',

      ['gettextCatalog', '$locale',
        function (gettextCatalog, $locale) {

          var locale = $locale.id.substring(0, 2);

          gettextCatalog.setCurrentLanguage(locale);
          gettextCatalog.debug = true;

          var service = {};

          service.setLocale = function (strLocale) {
            gettextCatalog.setCurrentLanguage(strLocale);
            locale = strLocale;
          };

          service.getText = function (str) {
            gettextCatalog.getString(str);
          };

          return service;
        }])

})();