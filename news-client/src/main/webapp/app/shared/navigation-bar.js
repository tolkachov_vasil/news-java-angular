(function () {
  'use strict';

  angular.module('news').component('navigationBar', {
    templateUrl: '/app/shared/navigation-bar.html',
    controller: ['localize',
      function (localize) {
        var vm = this;

        vm.setLocale = function (locale) {
          localize.setLocale(locale);
        }

      }]
  });

})();