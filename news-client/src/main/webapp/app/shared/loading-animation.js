(function () {
  'use strict';

  angular.module('news')

    .directive('loadingAnimation', ['$http', '$timeout',
      function ($http, $timeout) {
        return {
          scope: {},
          restrict: 'EA',
          link: function (scope, element) {
            scope.isLoading = function () {
              return $http.pendingRequests.length > 0;
            };
            scope.$watch(scope.isLoading, function (loading) {
              if (loading) {
                $timeout(function () {
                  if ($http.pendingRequests.length > 0) {
                    element.css({'display': 'block'});
                  }
                }, 500);
              } else {
                element.css({'display': 'none'});
              }
            });
          }
        };
      }]);
})();