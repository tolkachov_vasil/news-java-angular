package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.dto.BriefNewsPage;
import com.epam.newsmanagement.dto.NewsDetail;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.List;

@Controller
@RestController
@RequestMapping(value = "/api")
public class MainController {

    private final int PAGE_SIZE = 5;

    @Autowired
    private Logger LOG;

    @Autowired
    private TagService tagService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private NewsService newsService;

    @Autowired
    private CommentService commentService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/news/{id}", method = RequestMethod.GET)
    public NewsDetail getNewsPage(@PathVariable long id) {
         LOG.debug("request news id:" + id);
        return newsService.getNewsPage(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/tag/all", method = RequestMethod.GET)
    public List<Tag> getAllTags() {
         LOG.debug("request all tags");
        return tagService.getAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/author/all", method = RequestMethod.GET)
    public List<Author> getAllAuthors() {
         LOG.debug("request all authors");
        return authorService.getAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/page/{pageNumber}", method = RequestMethod.GET)
    public BriefNewsPage getPage(@PathVariable Integer pageNumber) {
         LOG.debug("request page num:" + pageNumber);
        return newsService.getBriefNewsPage(
                pageNumber, PAGE_SIZE,
                newsService.getNewsSequence());
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/page/{pageNumber}/author/{authorId}", method = RequestMethod.GET)
    public BriefNewsPage getPageByAuthorId(
            @PathVariable Integer pageNumber,
            @PathVariable Long authorId) {
        return newsService.getBriefNewsPage(
                pageNumber, PAGE_SIZE,
                newsService.getNewsSequence(authorId));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/page/{pageNumber}/tags/{tagsId}", method = RequestMethod.GET)
    public BriefNewsPage getPageByTagsId(
            @PathVariable Integer pageNumber,
            @PathVariable List<Long> tagsId) {
        return newsService.getBriefNewsPage(
                pageNumber, PAGE_SIZE,
                newsService.getNewsSequence(tagsId));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/page/{pageNumber}/author/{authorId}/tags/{tagsId}", method = RequestMethod.GET)
    public BriefNewsPage getPageByAuthorIdAndTagsId(
            @PathVariable Integer pageNumber,
            @PathVariable Long authorId,
            @PathVariable List<Long> tagsId) {
        return newsService.getBriefNewsPage(
                pageNumber, PAGE_SIZE,
                newsService.getNewsSequence(authorId, tagsId));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    public Comment addComment(@RequestBody Comment comment) {
         LOG.debug("request add new comment");
        comment.setCreationDate(new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));
        commentService.add(comment);
        return comment;
    }

}