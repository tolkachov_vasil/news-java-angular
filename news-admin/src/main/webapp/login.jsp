<!--###LOGIN marker-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="fragment" content="!"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>News Portal - Login</title>
    <meta name="viewport" content="height=device-height,width=device-width,user-scalable=yes">
    <link rel="stylesheet" href="css/login.css">
    <style>

    </style>
</head>
<body>
<div class="LoginForm">
    <form id="form" action="/j_spring_security_check" method="post">
        <h2>Please sign in</h2>
        <div id="error">
            Invalid username or password.<br>
        </div>
        <div id="logout">
            You have been logged out.<br>
        </div>
        <input type="hidden"
               name="${_csrf.parameterName}"
               value="${_csrf.token}"/>
        <input id="login" type="text" name="j_username" placeholder="Login" required autofocus>
        <input id="password" type="password" name="j_password" placeholder="Password" required>
        <input id="button" type="submit" value="Login"/>
        ${error}
    </form>
</div>
</body>
<script>
    (function () {
        if (window.location.href.indexOf("error") > -1) {
            document.getElementById('error').style.display = 'block';
            document.getElementById('login').style.borderColor = 'red';
            document.getElementById('password').style.borderColor = 'red';
        } else if (window.location.href.indexOf("logout") > -1) {
            document.getElementById('logout').style.display = 'block';
        }
    })();
</script>
</html>