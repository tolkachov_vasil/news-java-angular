(function () {
  'use strict';

  angular.module('news')

    .component('newsEditAuthors', {
      templateUrl: '/app/edit_authors/edit_authors.html',
      controller: ['authorFactory',
        function (authorFactory) {
          var vm = this;
          vm.authors = authorFactory.authors;

          vm.addAuthor = function (author, form) {
            if (form.$valid) {
              authorFactory.save(author);
            }
          };

          vm.updateAuthor = function (author) {
            delete author.edit;
            authorFactory.update(author);
          };

          vm.deleteAuthor = function (id) {
            authorFactory.remove(id);
          }

        }]
    })

    .config(['$stateProvider', function ($stateProvider) {
      $stateProvider.state('news.edit_authors', {
        url: 'authors/',
        template: '<news-edit-authors authors="authors"></news-edit-authors>'
      });
    }]);

})();