(function () {
  'use strict';

  angular.module('news').component('navigationMenu', {
    templateUrl: '/app/shared/navigation-menu.html'
  });

})();