(function () {
  'use strict';

  angular.module('news')
    .component('navigationBar', {
      templateUrl: '/app/shared/navigation-bar.html',
      controller: ['localize', '$http', '$window', '$rootScope',
        function (localize, $http, $window, $rootScope) {
          var vm = this;
          vm.userName = $rootScope.userName;

          vm.setLocale = function (locale) {
            localize.setLocale(locale);
          };

          vm.doLogout = function () {
            $http.post('/logout');
            $window.location.href = '/login.jsp?logout';
          }

        }]
    });

})();