(function () {
  'use strict';

  angular.module('news')

    .component('pageView', {
      templateUrl: '/app/list/page/page.html',
      bindings: {page: '<'},
      controller: ['newsFactory', '$state', function (newsFactory, $state) {
        var vm = this;
        var toDel = [];
        vm.deleteNews = function () {
          vm.page.content.forEach(function (news) {
            if (news.selected) {
              toDel.push(news.newsId);
            }
          });
          newsFactory.delete(toDel)
            .then(function () {
              $state.go('news.list', {pageNum: 1}, {reload: true});
            });
        };

      }]
    });

})();