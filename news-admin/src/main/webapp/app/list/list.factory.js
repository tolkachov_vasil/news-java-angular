(function () {
  'use strict';
  angular.module('news').factory('newsListFactory', newsListFactory);

  newsListFactory.$inject = ['$resource', '$q'];

  function newsListFactory($resource, $q) {
    var service = {};

    service.loadPage = function (pageNum) {
      return $resource('api/page/:pageNum').get({pageNum: pageNum}).$promise;
    };

    return service;
  }

})();
