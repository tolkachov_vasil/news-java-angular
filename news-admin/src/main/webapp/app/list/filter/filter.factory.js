(function () {
  'use strict';

  angular.module('news').factory('filterFactory', filterFactory);
  angular.module('news').factory('filterFactoryUrl', filterFactoryUrl);

  filterFactory.$inject = ['tagFactory', 'authorFactory', '$state', 'filterFactoryUrl'];

  function filterFactory(tagFactory, authorFactory, $state, filterFactoryUrl) {
    var service = {};

    service.filter = {
      tags: tagFactory.tags,
      authors: authorFactory.authors,
      selectedAuthor: '',
      selectedTags: []
    };

    service.resetFilter = resetFilter;
    service.updateSelectedTags = updateSelectedTags;
    service.applyFilter = applyFilter;

    function resetFilter() {
      service.filter.selectedAuthor = '';
      service.filter.tags.forEach(function (tag) {
        delete(tag.enabled);
      });
      service.filter.selectedTags = [];
      applyFilter();
    }

    function updateSelectedTags() {
      service.filter.selectedTags = [];
      service.filter.tags.forEach(function (tag) {
        if (tag.enabled) {
          service.filter.selectedTags.push(tag);
        }
      });
    }

    function applyFilter() {

      updateSelectedTags();

      updateFilterUrl();
      $state.go('news.list', {pageNum: 1}, {reload: true});
    }

    function updateFilterUrl() {
      var tagIds = [];
      service.filter.selectedTags.forEach(function (tag) {
        tagIds.push(tag.tagId);
      });
      var authorId = service.filter.selectedAuthor.authorId || 0;
      if (tagIds.length !== 0 && authorId === 0) {
        filterFactoryUrl.setFilterUrl('/tags/' + tagIds.join()); // tags only
      } else if (tagIds.length === 0 && authorId !== 0) {
        filterFactoryUrl.setFilterUrl('/author/' + authorId); // author only
      } else if (tagIds.length !== 0 && authorId !== 0) {
        filterFactoryUrl.setFilterUrl('/author/' + authorId + '/tags/' + tagIds.join()); // tags & author
      } else
        filterFactoryUrl.setFilterUrl(''); // no filter
    }

    return service;
  }


  function filterFactoryUrl() {
    var filterUrl = '';
    var service = {};

    service.getFilterUrl = function () {
      return filterUrl;
    };

    service.setFilterUrl = function (url) {
      filterUrl = url;
    };

    return service;
  }

})();