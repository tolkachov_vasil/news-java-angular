(function () {
  'use strict';

  angular.module('news')

    .component('newsListView', {
      templateUrl: '/app/list/list.html',
      bindings: {page: '<'}
    })

    .config(['$stateProvider', function ($stateProvider) {
      $stateProvider
        .state('news.list', {
          url: 'page/:pageNum',
          templateUrl: '/app/list/list.html',
          resolve: {
            page: ['newsListFactory', '$stateParams',
              function (newsListFactory, $stateParams) {
                return newsListFactory.loadPage($stateParams.pageNum);
              }]
          },
          controller: ['$scope', 'page', function ($scope, page) {
            $scope.page = page;
          }]
        });
    }]);


})();