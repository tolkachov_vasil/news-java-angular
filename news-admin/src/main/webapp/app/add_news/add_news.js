(function () {
  'use strict';

  angular.module('news')

    .component('newsAddNews', {
      templateUrl: '/app/add_news/add_news.html',
      controller: ['newsFactory', 'filterFactory', '$state',
        function (newsFactory, filterFactory, $state) {
          var vm = this;
          vm.news = {
            title: '',
            shortText: '',
            fullText: ''
          };
          vm.filter = filterFactory.filter;

          vm.addNews = function (news, form) {
            if (form.$valid && vm.filter.selectedAuthor !== '' && vm.filter.selectedTags.length > 0) {
              news.tags = vm.filter.selectedTags;
              news.author = vm.filter.selectedAuthor;
              newsFactory.save(news)
                .then(function () {
                  filterFactory.resetFilter();
                  $state.go('news.list', {pageNum: 1}, {reload: true});
                });
            }
          };

        }]
    })

    .config(['$stateProvider', function ($stateProvider) {
      $stateProvider.state('news.add_news', {
        url: 'news/add',
        template: '<news-add-news></news-add-news>'
      });
    }]);

})();