(function () {
  'use strict';

  angular.module('news')

    .component('mainView', {
      templateUrl: '/app/main/main.html',
      controller: function () {
      }
    })

    .config(['$stateProvider', function ($stateProvider) {
      $stateProvider
        .state('news', {
          url: '/',
          abstract: true,
          template: '<main-view></main-view>'
        });
    }]);

})();