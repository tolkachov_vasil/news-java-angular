(function () {
  'use strict';

  angular.module('news')

    .config(['$httpProvider', function ($httpProvider) {

      // login redirect interceptor
      $httpProvider.interceptors.push(
        ['$q', '$window', function ($q, $window) {
          return {
            response: function (response) {
              if (typeof response.data === 'string' && response.data.indexOf("###LOGIN") > -1) {
                $window.location.href = "/login.jsp";
                return $q.reject(response);
              } else {
                return response;
              }
            }
          };
        }]);

    }])

    .run(['$http', '$rootScope', '$window',
      function ($http, $rootScope, $window) {
        $http.get('/user')
          .then(function (resp) {
            $rootScope.userName = resp.data.userName;
          })
          .catch(function () {
            $rootScope.userName = '';
            $window.location.href = '/login.jsp';
          });
      }])

})();