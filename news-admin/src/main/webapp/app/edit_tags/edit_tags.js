(function () {
  'use strict';

  angular.module('news')

    .component('newsEditTags', {
      templateUrl: '/app/edit_tags/edit_tags.html',
      controller: ['tagFactory',
        function (tagFactory) {
          var vm = this;
          vm.tags = tagFactory.tags;

          vm.addTag = function (tag, form) {
            if (form.$valid) {
              tagFactory.save(tag);
            }
          };

          vm.updateTag = function (tag) {
            delete tag.edit;
            tagFactory.update(tag);
          };

          vm.deleteTag = function (id) {
            tagFactory.remove(id);
          }

        }]
    })

    .config(['$stateProvider', function ($stateProvider) {
      $stateProvider.state('news.edit_tags', {
        url: 'tags/',
        template: '<news-edit-tags tags="tags"></news-edit-tags>'
      });
    }]);

})();