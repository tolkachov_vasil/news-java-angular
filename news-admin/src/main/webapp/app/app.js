(function () {
  'use strict';

  angular.module('news', ['ngResource', 'ui.router', 'once', 'gettext'])

    .config([
      '$urlRouterProvider',
      '$locationProvider',
      function ($urlRouterProvider, $locationProvider) {
        $urlRouterProvider.otherwise('/page/1');
        $locationProvider.hashPrefix('!');
      }
    ])

    .run(['$rootScope', '$state', '$stateParams',
      function ($rootScope, $state, $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        /*$rootScope.$on('$stateChangeStart', function () {
         angular.element(document.getElementsByTagName('body')).attr('data-status', 'loading');
         });

         $rootScope.$on('$stateChangeSuccess', function () {
         angular.element(document.getElementsByTagName('body')).attr('data-status', 'ready');
         });*/
      }
    ]);

})();
