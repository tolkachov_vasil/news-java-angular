(function () {
  'use strict';

  angular.module('news').factory('newsFactory', newsFactory);

  newsFactory.$inject = ['$resource'];

  function newsFactory($resource) {

    var service = {};

    service.loadNews = function (id) {
      return $resource('api/news/:id').get({id: id}).$promise;
    };

    service.save = function (news) {
      return $resource('api/news').save(news).$promise;
    };

    service.delete = function (ids) {
      return $resource('api/news/:ids').remove({ids: ids}).$promise;
    };

    return service;
  }
})();