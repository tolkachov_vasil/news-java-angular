(function () {

  angular.module('news').factory('authorFactory', authorFactory);

  authorFactory.$inject = ['$resource'];

  function authorFactory($resource) {
    var service = {};
    service.authors = $resource('api/author/all').query();
    var selected = null;


    service.select = function (author) {
      selected = author;
    };

    service.unSelect = function () {
      selected = {authorName: 'All authors', authorId: 0};
    };

    service.getSelected = function () {
      return selected;
    };

    service.save = function (author) {
      $resource('api/author').save(author).$promise
        .then(function (newAuthor) {
          service.authors.push(newAuthor);
          author.authorName = '';
        });
    };

    service.update = function (author) {
      $resource('api/author').save(author);
    };

    service.remove = function (id) {
      $resource('api/author/:id').remove({id: id});
      service.authors.forEach(function (a, i) {
        if (a.authorId === id) {
          service.authors.splice(i, 1);
        }
      })
    };

    return service;
  }
})();