(function () {
  'use strict';

  angular.module('news').factory('tagFactory', tagFactory);

  tagFactory.$inject = ['$resource'];

  function tagFactory($resource) {
    var service = {};

    service.tags = $resource('api/tag/all').query();

    service.getAll = function () {
      return tags;
    };

    service.save = function (tag) {
      $resource('api/tag').save(tag).$promise
        .then(function (newTag) {
          service.tags.push(newTag);
          tag.tagName = '';
        });
    };

    service.update = function (tag) {
      $resource('api/tag').save(tag);
    };

    service.remove = function (id) {
      $resource('api/tag/:id').remove({id: id});
      service.tags.forEach(function (t, i) {
        if (t.tagId === id) {
          service.tags.splice(i, 1);
        }
      })
    };

    return service;
  }
})();