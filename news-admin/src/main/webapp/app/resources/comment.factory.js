(function () {

  angular.module('news').factory('commentFactory', commentFactory);

  commentFactory.$inject = ['$resource'];

  function commentFactory($resource) {

    var service = {};

    service.save = function (newsId, text) {
      var comment = {
        commentText: text,
        newsId: newsId,
        creationDate: new Date()
      };
      return $resource('api/comment').save(comment).$promise;
    };

    service.delete = function (id) {
      return $resource('api/comment/:id').remove({id: id}).$promise;
    };

    return service;
  }
})();