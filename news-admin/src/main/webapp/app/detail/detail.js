(function () {
  'use strict';

  angular.module('news')

    .component('newsDetailView', {
      templateUrl: '/app/detail/detail.html',
      bindings: {news: '<', back: '<'},
      controller: ['commentFactory',
        function (commentFactory) {
          var vm = this;

          vm.saveComment = function (data, addCommentForm) {
            if (addCommentForm.$valid) {
              commentFactory.save(vm.news.news.newsId, data.commentText)
                .then(function (newComment) {
                  data.commentText = '';
                  vm.news.comments.push(newComment);
                })
            }
          };

          vm.deleteComment = function (id) {
            commentFactory.delete(id);
            vm.news.comments.forEach(function (c, i) {
              if (c.commentId === id) {
                vm.news.comments.splice(i, 1);
              }
            });

          };
        }]
    })

    .config(['$stateProvider', function ($stateProvider) {
      $stateProvider.state('news.detail', {
        url: 'page/:pageNum/news/:newsId',
        template: '<news-detail-view news="news" back="back"></news-detail-view>',
        resolve: {
          news: ['newsFactory', '$stateParams',
            function (newsFactory, $stateParams) {
              return newsFactory.loadNews($stateParams.newsId);
            }]
        },
        controller: ['$scope', 'news', '$stateParams',
          function ($scope, news, $stateParams) {
            $scope.news = news;
            $scope.back = $stateParams.pageNum;
          }]
      });
    }]);

})();