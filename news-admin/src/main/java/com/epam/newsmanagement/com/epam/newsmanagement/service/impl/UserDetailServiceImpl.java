package com.epam.newsmanagement.com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.RoleDao;
import com.epam.newsmanagement.dao.UserDao;
import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.entity.User;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    UserDao userDao;

    @Autowired
    RoleDao roleDao;

    @Autowired
    Logger LOG;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        LOG.debug(s);
        User user = userDao.findByName(s);
        if (user == null) {
            throw new UsernameNotFoundException("name error");
        }
        Set<GrantedAuthority> roles = new HashSet<>();
        Role role = roleDao.findByUserId(user.getUserId());
        roles.add(new SimpleGrantedAuthority(role.getRoleName()));
        LOG.debug("roles" + roles.toString());

        return new org.springframework.security.core.userdetails.User(
                user.getLogin(),
                user.getPassword(),
                roles);
    }

}
