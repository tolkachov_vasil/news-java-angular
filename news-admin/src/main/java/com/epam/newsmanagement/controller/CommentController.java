package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.service.CommentService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;

@Controller
@RestController
@RequestMapping(value = "/api")
public class CommentController {

    @Autowired
    private Logger LOG;

    @Autowired
    private CommentService commentService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    public Comment saveComment(@RequestBody Comment comment) {
        LOG.debug("text:" + comment.getCommentText());
        comment.setCreationDate(new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));
        commentService.edit(comment);
        return comment;
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(
            value = "/comment/{commentId}",
            method = RequestMethod.DELETE)
    public Long deleteComment(@PathVariable Long commentId) {
        LOG.debug("id:" + commentId);
        commentService.delete(commentId);
        return commentId;
    }

}