package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.TagService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RestController
@RequestMapping(value = "/api")
public class TagController {

    @Autowired
    private Logger LOG;

    @Autowired
    private TagService tagService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(
            value = "/tag/all",
            method = RequestMethod.GET)
    public List<Tag> getAllTags() {
        LOG.debug("");
        return tagService.getAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(
            value = "/tag",
            method = RequestMethod.POST)
    public Tag saveTag(@RequestBody Tag tag) {
        LOG.debug("name:" + tag.getTagName());
        tagService.edit(tag);
        return tag;
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(
            value = "/tag/{id}",
            method = RequestMethod.DELETE)
    public Long deleteTag(@PathVariable Long id) {
        LOG.debug("id:" + id);
        tagService.delete(id);
        return id;
    }

}