package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.service.AuthorService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

@Controller
@RestController
@RequestMapping(value = "/api")
public class AuthorController {

    @Autowired
    private Logger LOG;

    @Autowired
    private AuthorService authorService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(
            value = "/author/all",
            method = RequestMethod.GET)
    public List<Author> getAllAuthors() {
        LOG.debug("");
        return authorService.getAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(
            value = "/author",
            method = RequestMethod.POST)
    public Author saveAuthor(@RequestBody Author author) {
        LOG.debug("name:" + author.getAuthorName());
        authorService.edit(author);
        return author;
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(
            value = "/author/{id}",
            method = RequestMethod.DELETE)
    public Long expireAuthor(@PathVariable Long id) {
        Author author = authorService.getById(id);
        LOG.debug("name:" + author.getAuthorName());
        Timestamp now = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        author.setExpired(now);
        authorService.edit(author);
        return id;
    }

}