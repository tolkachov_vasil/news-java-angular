package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.dto.BriefNewsPage;
import com.epam.newsmanagement.dto.NewsDetail;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.List;

@Controller
@RestController
@RequestMapping(value = "/api")
public class NewsController {

    private final int PAGE_SIZE = 5;

    @Autowired
    private Logger LOG;

    @Autowired
    private NewsService newsService;

    @Autowired
    private CommentService commentService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(
            value = "/news/{id}",
            method = RequestMethod.GET)
    public NewsDetail getNewsPage(@PathVariable Long id) {
        LOG.debug(" id:" + id);
        return newsService.getNewsPage(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(
            value = "/page/{pageNumber}",
            method = RequestMethod.GET)
    public BriefNewsPage getPage(@PathVariable Integer pageNumber) {
        LOG.debug(" num:" + pageNumber);
        return newsService.getBriefNewsPage(
                pageNumber, PAGE_SIZE,
                newsService.getNewsSequence());
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(
            value = "/page/{pageNumber}/author/{authorId}",
            method = RequestMethod.GET)
    public BriefNewsPage getPageByAuthorId(
            @PathVariable Integer pageNumber,
            @PathVariable Long authorId) {
        LOG.debug(" id:" + authorId);
        return newsService.getBriefNewsPage(
                pageNumber, PAGE_SIZE,
                newsService.getNewsSequence(authorId));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(
            value = "/page/{pageNumber}/tags/{tagsId}",
            method = RequestMethod.GET)
    public BriefNewsPage getPageByTagsId(
            @PathVariable Integer pageNumber,
            @PathVariable List<Long> tagsId) {
        LOG.debug(" id:" + tagsId);
        return newsService.getBriefNewsPage(
                pageNumber, PAGE_SIZE,
                newsService.getNewsSequence(tagsId));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(
            value = "/page/{pageNumber}/author/{authorId}/tags/{tagsId}",
            method = RequestMethod.GET)
    public BriefNewsPage getPageByAuthorIdAndTagsId(
            @PathVariable Integer pageNumber,
            @PathVariable Long authorId,
            @PathVariable List<Long> tagsId) {
        LOG.debug(" authorId:" + authorId + " tagsId:" + tagsId);
        return newsService.getBriefNewsPage(
                pageNumber, PAGE_SIZE,
                newsService.getNewsSequence(authorId, tagsId));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/news", method = RequestMethod.POST)
    public News editNews(@RequestBody News news) {
        LOG.debug("news:" + news);
        news.setModificationDate(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
        news.setCreationDate(new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));
        newsService.edit(news);
        return news;
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(
            value = "/news/{newsIds}",
            method = RequestMethod.DELETE)
    public void deleteNews(@PathVariable List<Long> newsIds) {
        LOG.debug("ids:" + newsIds);
        commentService.deleteByNewsIds(newsIds);
        newsService.deleteList(newsIds);
    }

}