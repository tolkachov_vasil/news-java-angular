package com.epam.newsmanagement.config;

public class Constant {

    public static final String SQL_GET_NEWS_SEQ_PART1 = "" +
            "SELECT NEWS.NEWS_ID " +
            "FROM NEWS " +
            "LEFT JOIN COMMENTS ON (NEWS.NEWS_ID = COMMENTS.NEWS_ID) " +
            "JOIN NEWS_AUTHOR ON (NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID) " +
            "JOIN AUTHOR ON (AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID) " +
            "JOIN NEWS_TAG ON (NEWS.NEWS_ID = NEWS_TAG.NEWS_ID) " +
            "JOIN TAG ON (TAG.TAG_ID = NEWS_TAG.TAG_ID) ";

    public static final String SQL_GET_NEWS_SEQ_FILTER_WHERE =
            "WHERE ";

    public static final String SQL_GET_NEWS_SEQ_FILTER_AUTHOR =
            "NEWS_AUTHOR.AUTHOR_ID = :author_id ";

    public static final String SQL_GET_NEWS_SEQ_FILTER_AND =
            "AND ";

    public static final String SQL_GET_NEWS_SEQ_FILTER_TAGS =
            "TAG.TAG_ID IN :tag_ids ";

    public static final String SQL_GET_NEWS_SEQ_PART2 = "" +
            "GROUP BY NEWS.NEWS_ID, NEWS.MODIFICATION_DATE " +
            "ORDER BY NEWS.MODIFICATION_DATE DESC, COUNT(COMMENTS.COMMENT_ID) DESC";

    public static final String SQL_GET_PAGE = "" +
            "SELECT " +
            "NEWS.NEWS_ID AS newsId, " +
            "NEWS.TITLE AS title, " +
            "(SELECT AUTHOR_NAME " +
            "FROM AUTHOR " +
            "JOIN NEWS_AUTHOR ON AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID " +
            "WHERE NEWS_AUTHOR.NEWS_ID = NEWS.NEWS_ID) AS author, " +
            "(SELECT COUNT(COMMENT_ID) " +
            "FROM COMMENTS " +
            "WHERE COMMENTS.NEWS_ID = NEWS.NEWS_ID) AS comments, " +
            "(concatenate_list(CURSOR (SELECT TAG_NAME " +
            "FROM TAG " +
            "JOIN NEWS_TAG ON TAG.TAG_ID = NEWS_TAG.TAG_ID " +
            "WHERE NEWS.NEWS_ID = NEWS_TAG.NEWS_ID))) AS tags, " +
            "NEWS.SHORT_TEXT AS brief, " +
            "NEWS.MODIFICATION_DATE AS newsDate " +
            "FROM NEWS " +
            "WHERE NEWS_ID IN :seq ";

}