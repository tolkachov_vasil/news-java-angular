package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentDao commentDao;

    @Override
    public Comment add(Comment comment) {
        return commentDao.saveAndFlush(comment);
    }

    @Override
    public void delete(long id) {
        commentDao.delete(id);
    }

    @Override
    public Comment getById(long id) {
        return commentDao.findOne(id);
    }

    @Override
    public Comment edit(Comment comment) {
        return commentDao.saveAndFlush(comment);
    }

    @Override
    public List<Comment> getAll() {
        return commentDao.findAll();
    }

    @Override
    public void deleteByNewsIds(List<Long> id) {
        commentDao.deleteByNewsIds(id);
    }
}
