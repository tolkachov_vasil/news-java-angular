package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.RoleDao;
import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDao roleDao;

    @Override
    public Role add(Role role) {
        return roleDao.saveAndFlush(role);
    }

    @Override
    public void delete(long id) {
        roleDao.delete(id);
    }

    @Override
    public Role getById(long id) {
        return roleDao.findOne(id);
    }

    @Override
    public Role edit(Role role) {
        return roleDao.saveAndFlush(role);
    }

    @Override
    public List<Role> getAll() {
        return roleDao.findAll();
    }
}
