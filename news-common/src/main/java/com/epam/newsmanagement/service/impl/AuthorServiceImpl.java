package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorServiceImpl implements AuthorService {

    @Autowired
    private AuthorDao authorDao;

    @Override
    public Author add(Author author) {
        return authorDao.saveAndFlush(author);
    }

    @Override
    public void delete(long id) {
        authorDao.delete(id);
    }

    @Override
    public Author getById(long id) {
        return authorDao.findOne(id);
    }

    @Override
    public Author edit(Author author) {
        return authorDao.saveAndFlush(author);
    }

    @Override
    public List<Author> getAll() {
        return authorDao.findAll();
    }
}
