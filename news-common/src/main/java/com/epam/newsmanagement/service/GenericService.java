package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.IEntity;

import java.util.List;

public interface GenericService<T extends IEntity> {

    T add(T entity);

    void delete(long id);

    T getById(long id);

    T edit(T entity);

    List<T> getAll();
}

