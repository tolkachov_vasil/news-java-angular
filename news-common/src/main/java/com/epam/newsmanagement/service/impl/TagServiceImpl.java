package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    @Autowired
    TagDao tagDao;

    @Override
    public Tag add(Tag tag) {
        return tagDao.saveAndFlush(tag);
    }

    @Override
    public void delete(long id) {
        tagDao.delete(id);
    }

    @Override
    public Tag getById(long id) {
        return tagDao.findOne(id);
    }

    @Override
    public Tag edit(Tag tag) {
        return tagDao.saveAndFlush(tag);
    }

    @Override
    public List<Tag> getAll() {
        return tagDao.findAll();
    }
}
