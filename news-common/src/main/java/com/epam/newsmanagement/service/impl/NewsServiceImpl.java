package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.config.Constant;
import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.dto.BriefNews;
import com.epam.newsmanagement.dto.BriefNewsPage;
import com.epam.newsmanagement.dto.NewsDetail;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.service.NewsService;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.StringType;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class NewsServiceImpl implements NewsService {

    @PersistenceContext
    EntityManager em;

    @Autowired
    private Logger LOG;

    @Autowired
    private NewsDao newsDao;

    @Autowired
    private CommentDao commentDao;

    @Override
    public News add(News news) {
        return newsDao.saveAndFlush(news);
    }

    @Override
    public void delete(long id) {
        newsDao.delete(id);
    }

    @Override
    public News getById(long id) {
        return newsDao.findOne(id);
    }

    @Override
    public News edit(News news) {
        return newsDao.saveAndFlush(news);
    }

    @Override
    public List<News> getAll() {
        return newsDao.findAll();
    }

    @Override
    @Transactional
    public List<Long> getNewsSequence() {
        Session session = (Session) em.getDelegate();
        Query query = session.createSQLQuery(""
                + Constant.SQL_GET_NEWS_SEQ_PART1
                + Constant.SQL_GET_NEWS_SEQ_PART2)
                .addScalar("news_id", StandardBasicTypes.LONG);

        @SuppressWarnings("unchecked")
        List<Long> list = query
                .list();
        LOG.debug(list.toString());
        return list;
    }

    @Override
    @Transactional
    public List<Long> getNewsSequence(Long authorId) {
        Session session = (Session) em.getDelegate();
        Query query = session.createSQLQuery(""
                + Constant.SQL_GET_NEWS_SEQ_PART1
                + Constant.SQL_GET_NEWS_SEQ_FILTER_WHERE
                + Constant.SQL_GET_NEWS_SEQ_FILTER_AUTHOR
                + Constant.SQL_GET_NEWS_SEQ_PART2)
                .addScalar("news_id", StandardBasicTypes.LONG);
        @SuppressWarnings("unchecked")
        List<Long> list = query
                .setParameter("author_id", authorId)
                .list();
        LOG.debug(list.toString());
        return list;
    }

    @Override
    @Transactional
    public List<Long> getNewsSequence(Long authorId, List<Long> tagId) {
        Session session = (Session) em.getDelegate();
        Query query = session.createSQLQuery(""
                + Constant.SQL_GET_NEWS_SEQ_PART1
                + Constant.SQL_GET_NEWS_SEQ_FILTER_WHERE
                + Constant.SQL_GET_NEWS_SEQ_FILTER_AUTHOR
                + Constant.SQL_GET_NEWS_SEQ_FILTER_AND
                + Constant.SQL_GET_NEWS_SEQ_FILTER_TAGS
                + Constant.SQL_GET_NEWS_SEQ_PART2)
                .addScalar("news_id", StandardBasicTypes.LONG);
        @SuppressWarnings("unchecked")
        List<Long> list = query
                .setParameter("author_id", authorId)
                .setParameterList("tag_ids", tagId)
                .list();
        LOG.debug(list.toString());
        return list;
    }

    @Override
    @Transactional
    public List<Long> getNewsSequence(List<Long> tagId) {
        Session session = (Session) em.getDelegate();
        Query query = session.createSQLQuery(""
                + Constant.SQL_GET_NEWS_SEQ_PART1
                + Constant.SQL_GET_NEWS_SEQ_FILTER_WHERE
                + Constant.SQL_GET_NEWS_SEQ_FILTER_TAGS
                + Constant.SQL_GET_NEWS_SEQ_PART2)
                .addScalar("news_id", StandardBasicTypes.LONG);
        @SuppressWarnings("unchecked")
        List<Long> list = query
                .setParameterList("tag_ids", tagId)
                .list();
        LOG.debug(list.toString());
        return list;
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public BriefNewsPage getBriefNewsPage(int pageNumber, int pageSize, List<Long> sequence) {
        LOG.debug("no:" + pageNumber);
        Session session = (Session) em.getDelegate();
        Query query = session.createSQLQuery(Constant.SQL_GET_PAGE)
                .addScalar("title", StringType.INSTANCE)
                .addScalar("brief", StringType.INSTANCE)
                .addScalar("author", StringType.INSTANCE)
                .addScalar("tags", StringType.INSTANCE)
                .addScalar("newsId", StandardBasicTypes.LONG)
                .addScalar("comments", StandardBasicTypes.INTEGER)
                .addScalar("newsDate", StandardBasicTypes.DATE);
        query.setResultTransformer(Transformers.aliasToBean(BriefNews.class));

        int fullLen = sequence.size();
        int firstIndex = Math.min((pageNumber - 1) * pageSize, fullLen);
        int lastIndex = Math.min(pageNumber * pageSize, fullLen);
        int totalPages = fullLen / pageSize;
        if (fullLen % pageSize > 0) {
            totalPages++;
        }
        List<Long> pageList = sequence.subList(firstIndex, lastIndex);
        List<BriefNews> list = null;
        if (fullLen > 0) {
            list = query
                    .setParameterList("seq", pageList)
                    .list();
        }
        BriefNewsPage page = new BriefNewsPage();
        page.setContent(list);
        page.setNumber(pageNumber);
        page.setPageSize(pageSize);
        page.setTotalPages(totalPages);
        return page;
    }

    @Override
    @Transactional
    public NewsDetail getNewsPage(Long newsId) {
        LOG.debug("id:" + newsId);
        NewsDetail newsDetail = new NewsDetail();
        long next = 0;
        long prev = 0;
        News news = null;
        List<Long> sequence = getNewsSequence();
        int fullLen = sequence.size();
        int idx = sequence.indexOf((newsId));
        if (idx >= 0) {
            news = getById(sequence.get(idx));

            LOG.debug(news.toString());
            if (idx > 0) {
                prev = sequence.get(idx - 1);
            }
            if (idx + 1 < fullLen) {
                next = sequence.get(idx + 1);
            }
        }
        newsDetail.setNextNewsId(next);
        newsDetail.setPrevNewsId(prev);
        newsDetail.setNews(news);
        newsDetail.setComments(commentDao.findByNewsId(newsId));
        return newsDetail;
    }

    @Override
    public void deleteList(List<Long> id) {
        newsDao.deleteByIds(id);
    }

}
