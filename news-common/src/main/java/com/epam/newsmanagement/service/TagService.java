package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Tag;

public interface TagService extends GenericService<Tag> {

}
