package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.UserDao;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.service.UserService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Autowired
    Logger LOG;

    @Override
    public User add(User news) {
        return userDao.saveAndFlush(news);
    }

    @Override
    public void delete(long id) {
        userDao.delete(id);
    }

    @Override
    public User getById(long id) {
        return userDao.findOne(id);
    }

    @Override
    public User edit(User news) {
        return userDao.saveAndFlush(news);
    }

    @Override
    public List<User> getAll() {
        return userDao.findAll();
    }

}
