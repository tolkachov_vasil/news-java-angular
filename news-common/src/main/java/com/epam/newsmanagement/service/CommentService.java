package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Comment;

import java.util.List;

public interface CommentService extends GenericService<Comment> {

    void deleteByNewsIds(List<Long> id);

}
