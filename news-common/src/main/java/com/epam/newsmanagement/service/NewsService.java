package com.epam.newsmanagement.service;

import com.epam.newsmanagement.dto.BriefNewsPage;
import com.epam.newsmanagement.dto.NewsDetail;
import com.epam.newsmanagement.entity.News;

import java.util.List;

public interface NewsService extends GenericService<News> {

    List<Long> getNewsSequence();

    List<Long> getNewsSequence(Long authorId);

    List<Long> getNewsSequence(Long authorId, List<Long> tagId);

    List<Long> getNewsSequence(List<Long> tagId);

    BriefNewsPage getBriefNewsPage(int pageNumber, int pageSize, List<Long> sequence);

    NewsDetail getNewsPage(Long newsId);

    void deleteList(List<Long> id);

}
