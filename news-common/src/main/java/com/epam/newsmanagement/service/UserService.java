package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.User;

public interface UserService extends GenericService<User> {

}
