package com.epam.newsmanagement.utils;

import com.epam.newsmanagement.config.AppConfig;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.service.NewsService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class Runner {

    @Autowired
    private Logger LOG;

    @Autowired
    private NewsService newsService;

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        Runner runner = context.getBean(Runner.class);
        runner.run();
    }

    public void run() {
        LOG.debug("-------------------logger-----------------");
        News news = newsService.getById(1);
        System.out.println(news);
    }

}
