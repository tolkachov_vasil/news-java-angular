package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends JpaRepository<User, Long> {

    @Query("select u from User u where u.userName = :name")
    User findByName(@Param("name") String name);

}
