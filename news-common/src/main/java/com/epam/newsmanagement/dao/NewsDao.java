package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface NewsDao extends JpaRepository<News, Long> {

    @Modifying
    @Transactional
    @Query("delete from News n where n.newsId in ?1")
    void deleteByIds(List<Long> newsId);
}
