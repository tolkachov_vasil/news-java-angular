package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface CommentDao extends JpaRepository<Comment, Long> {

    @Query("select count(c) from Comment c where c.commentId = ?1")
    long countByNewsId(long newsId);

    @Query("select c from Comment c where c.newsId = ?1")
    List<Comment> findByNewsId(long newsId);

    @Modifying
    @Transactional
    @Query("delete from Comment c where c.newsId in ?1")
    void deleteByNewsIds(List<Long> newsId);

}
