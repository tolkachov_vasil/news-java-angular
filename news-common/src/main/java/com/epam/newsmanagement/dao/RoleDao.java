package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleDao extends JpaRepository<Role, Long> {

    @Query("select r from Role r where r.userId = :userId")
    Role findByUserId(@Param("userId") Long userId);
}
