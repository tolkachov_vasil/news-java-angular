package com.epam.newsmanagement.entity;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "NEWS")
@SequenceGenerator(name = "seq", sequenceName = "NEWS_SEQ")
public class News implements IEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    @Column(name = "NEWS_ID", precision = 20, scale = 0, nullable = false)
    private long newsId;
    @Column(name = "TITLE", length = 100, columnDefinition = "nvarchar2", nullable = false)
    private String title;
    @Column(name = "SHORT_TEXT", length = 200, columnDefinition = "nvarchar2", nullable = false)
    private String shortText;
    @Column(name = "FULL_TEXT", length = 4000, columnDefinition = "nvarchar2", nullable = false)
    private String fullText;
    @Column(name = "CREATION_DATE", length = 6, nullable = false)
    private Timestamp creationDate;
    @Column(name = "MODIFICATION_DATE", nullable = false)
    private Date modificationDate;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "NEWS_TAG",
            joinColumns = @JoinColumn(name = "NEWS_ID"),
            inverseJoinColumns = @JoinColumn(name = "TAG_ID"))
    private List<Tag> tags;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "NEWS_AUTHOR",
            joinColumns = @JoinColumn(name = "NEWS_ID"),
            inverseJoinColumns = @JoinColumn(name = "AUTHOR_ID"))
    private Author author;

    public News() {
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public long getNewsId() {
        return newsId;
    }

    public void setNewsId(long newsId) {
        this.newsId = newsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        News news = (News) o;

        if (newsId != news.newsId) {
            return false;
        }
        if (!title.equals(news.title)) {
            return false;
        }
        if (!shortText.equals(news.shortText)) {
            return false;
        }
        if (!fullText.equals(news.fullText)) {
            return false;
        }
        if (!creationDate.equals(news.creationDate)) {
            return false;
        }
        return modificationDate.equals(news.modificationDate);

    }

    @Override
    public int hashCode() {
        int result = (int) (newsId ^ (newsId >>> 32));
        result = 31 * result + title.hashCode();
        result = 31 * result + shortText.hashCode();
        result = 31 * result + fullText.hashCode();
        result = 31 * result + creationDate.hashCode();
        result = 31 * result + modificationDate.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "News{" +
                "newsId=" + newsId +
                ", title='" + title + '\'' +
                ", shortText='" + shortText + '\'' +
                ", fullText='" + fullText + '\'' +
                ", creationDate=" + creationDate +
                ", modificationDate=" + modificationDate +
                ", tags=" + tags +
                ", author=" + author +
                '}';
    }

}
