package com.epam.newsmanagement.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "AUTHOR")
@SequenceGenerator(name = "seq", sequenceName = "AUTHOR_SEQ")
public class Author implements IEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    @Column(name = "AUTHOR_ID", precision = 10, scale = 0, nullable = false)
    private long authorId;
    @Column(name = "AUTHOR_NAME", length = 30, columnDefinition = "nvarchar2", nullable = false)
    private String authorName;
    @Column(name = "EXPIRED", length = 6)
    private Timestamp expired;

    public Author() {
    }

    public Timestamp getExpired() {
        return expired;
    }

    public void setExpired(Timestamp creationDate) {
        this.expired = creationDate;
    }

    public long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(long authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Author author = (Author) o;

        if (authorId != author.authorId) {
            return false;
        }
        return authorName.equals(author.authorName);

    }

    @Override
    public int hashCode() {
        int result = (int) (authorId ^ (authorId >>> 32));
        result = 31 * result + authorName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Author{" +
                "authorId=" + authorId +
                ", authorName='" + authorName + '\'' +
                '}';
    }
}
