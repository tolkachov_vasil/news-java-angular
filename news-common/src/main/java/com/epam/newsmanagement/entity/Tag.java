package com.epam.newsmanagement.entity;

import javax.persistence.*;

@Entity
@Table(name = "TAG")
@SequenceGenerator(name = "seq", sequenceName = "TAG_SEQ")
public class Tag implements IEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    @Column(name = "TAG_ID", precision = 10, scale = 0, nullable = false)
    private long tagId;
    @Column(name = "TAG_NAME", length = 30, columnDefinition = "nvarchar2", nullable = false)
    private String tagName;

    public Tag() {
    }

    @Override
    public String toString() {
        return "Tag{" +
                "tagId=" + tagId +
                ", tagName='" + tagName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Tag tag = (Tag) o;

        if (tagId != tag.tagId) {
            return false;
        }
        return tagName.equals(tag.tagName);

    }

    @Override
    public int hashCode() {
        int result = (int) (tagId ^ (tagId >>> 32));
        result = 31 * result + tagName.hashCode();
        return result;
    }

    public long getTagId() {
        return tagId;
    }

    public void setTagId(long tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
}
