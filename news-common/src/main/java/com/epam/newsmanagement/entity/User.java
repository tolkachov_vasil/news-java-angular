package com.epam.newsmanagement.entity;

import javax.persistence.*;

@Entity
@Table(name = "USERS")
@SequenceGenerator(name = "seq", sequenceName = "USERS_SEQ")
public class User implements IEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    @Column(name = "USER_ID", precision = 10, scale = 0, nullable = false)
    private long userId;
    @Column(name = "USER_NAME", length = 50, columnDefinition = "nvarchar2", nullable = false)
    private String userName;
    @Column(name = "LOGIN", length = 30, columnDefinition = "nvarchar2", nullable = false)
    private String login;
    @Column(name = "PASSWORD", length = 30, columnDefinition = "nvarchar2", nullable = false)
    private String password;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        User user = (User) o;

        if (userId != user.userId) {
            return false;
        }
        if (!userName.equals(user.userName)) {
            return false;
        }
        if (!login.equals(user.login)) {
            return false;
        }
        return password.equals(user.password);

    }

    @Override
    public int hashCode() {
        int result = (int) (userId ^ (userId >>> 32));
        result = 31 * result + userName.hashCode();
        result = 31 * result + login.hashCode();
        result = 31 * result + password.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
