package com.epam.newsmanagement.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "COMMENTS")
@SequenceGenerator(name = "seq", sequenceName = "COMMENTS_SEQ")
public class Comment implements IEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    @Column(name = "COMMENT_ID", precision = 10, scale = 0, nullable = false)
    private long commentId;

    @Column(name = "NEWS_ID", precision = 10, nullable = false)
    private long newsId;

    @Column(name = "COMMENT_TEXT", length = 100, columnDefinition = "nvarchar2", nullable = false)
    private String commentText;

    @Column(name = "CREATION_DATE", length = 6, nullable = false)
    private Timestamp creationDate;

    public Comment() {
    }

    @Override
    public String toString() {
        return "Comment{" +
                "commentId=" + commentId +
                ", newsId=" + newsId +
                ", commentText='" + commentText + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Comment comment = (Comment) o;

        if (commentId != comment.commentId) {
            return false;
        }
        if (newsId != comment.newsId) {
            return false;
        }
        if (!commentText.equals(comment.commentText)) {
            return false;
        }
        return creationDate.equals(comment.creationDate);

    }

    @Override
    public int hashCode() {
        int result = (int) (commentId ^ (commentId >>> 32));
        result = 31 * result + (int) (newsId ^ (newsId >>> 32));
        result = 31 * result + commentText.hashCode();
        result = 31 * result + creationDate.hashCode();
        return result;
    }

    public long getCommentId() {

        return commentId;
    }

    public void setCommentId(long commentId) {
        this.commentId = commentId;
    }

    public long getNewsId() {
        return newsId;
    }

    public void setNewsId(long newsId) {
        this.newsId = newsId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

}

