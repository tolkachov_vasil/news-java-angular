package com.epam.newsmanagement.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ROLES")
public class Role implements IEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "USER_ID", precision = 10, nullable = false)
    private long userId;
    @Column(name = "ROLE_NAME", length = 20, columnDefinition = "nvarchar2", nullable = false)
    private String roleName;

    public Role() {
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Role role = (Role) o;

        if (userId != role.userId) {
            return false;
        }
        return roleName.equals(role.roleName);

    }

    @Override
    public int hashCode() {
        int result = (int) (userId ^ (userId >>> 32));
        result = 31 * result + roleName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Role{" +
                "userId=" + userId +
                ", roleName='" + roleName + '\'' +
                '}';
    }
}
