package com.epam.newsmanagement.dto;

import java.io.Serializable;
import java.util.List;

public class BriefNewsPage implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<BriefNews> content;
    private int totalPages;
    private int number;
    private int pageSize;

    public BriefNewsPage() {
    }

    public List<BriefNews> getContent() {
        return content;
    }

    public void setContent(List<BriefNews> content) {
        this.content = content;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BriefNewsPage that = (BriefNewsPage) o;

        if (totalPages != that.totalPages) {
            return false;
        }
        if (number != that.number) {
            return false;
        }
        if (pageSize != that.pageSize) {
            return false;
        }
        return content != null ? content.equals(that.content) : that.content == null;

    }

    @Override
    public int hashCode() {
        int result = totalPages;
        result = 31 * result + number;
        result = 31 * result + pageSize;
        return result;
    }

    @Override
    public String toString() {
        return "Page{" +
                "content=" + content +
                ", totalPages=" + totalPages +
                ", number=" + number +
                ", pageSize=" + pageSize +
                '}';
    }
}
