package com.epam.newsmanagement.dto;

import java.io.Serializable;
import java.sql.Date;

public class BriefNews implements Serializable {

    private static final long serialVersionUID = 1L;

    private long newsId;
    private String title;
    private Date newsDate;
    private String brief;
    private int comments;
    private String tags;
    private String author;

    public BriefNews() {
    }

    public long getNewsId() {
        return newsId;
    }

    public void setNewsId(long newsId) {
        this.newsId = newsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public Date getNewsDate() {
        return newsDate;
    }

    public void setNewsDate(Date newsDate) {
        this.newsDate = newsDate;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getComments() {
        return comments;
    }

    public void setComments(int comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BriefNews briefNews = (BriefNews) o;

        if (newsId != briefNews.newsId) {
            return false;
        }
        if (comments != briefNews.comments) {
            return false;
        }
        if (!title.equals(briefNews.title)) {
            return false;
        }
        if (!brief.equals(briefNews.brief)) {
            return false;
        }
        if (!newsDate.equals(briefNews.newsDate)) {
            return false;
        }
        if (!tags.equals(briefNews.tags)) {
            return false;
        }
        return author.equals(briefNews.author);

    }

    @Override
    public int hashCode() {
        int result = (int) (newsId ^ (newsId >>> 32));
        result = 31 * result + title.hashCode();
        result = 31 * result + brief.hashCode();
        result = 31 * result + newsDate.hashCode();
        result = 31 * result + tags.hashCode();
        result = 31 * result + author.hashCode();
        result = 31 * result + comments;
        return result;
    }

    @Override
    public String toString() {
        return "BriefNews{" +
                "newsId=" + newsId +
                ", title='" + title + '\'' +
                ", brief='" + brief + '\'' +
                ", newsDate=" + newsDate +
                ", tags='" + tags + '\'' +
                ", author='" + author + '\'' +
                ", comments=" + comments +
                "}";
    }

}
