package com.epam.newsmanagement.dto;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;

import java.io.Serializable;
import java.util.List;

public class NewsDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    private News news;
    private long nextNewsId;
    private long prevNewsId;
    private List<Comment> comments;

    public NewsDetail() {
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public long getNextNewsId() {
        return nextNewsId;
    }

    public void setNextNewsId(long nextNewsId) {
        this.nextNewsId = nextNewsId;
    }

    public long getPrevNewsId() {
        return prevNewsId;
    }

    public void setPrevNewsId(long prevNewsId) {
        this.prevNewsId = prevNewsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NewsDetail newsDetail = (NewsDetail) o;

        if (nextNewsId != newsDetail.nextNewsId) {
            return false;
        }
        if (prevNewsId != newsDetail.prevNewsId) {
            return false;
        }
        return news.equals(newsDetail.news);

    }

    @Override
    public int hashCode() {
        int result = news.hashCode();
        result = 31 * result + (int) (nextNewsId ^ (nextNewsId >>> 32));
        result = 31 * result + (int) (prevNewsId ^ (prevNewsId >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "NewsDetail{" +
                "news=" + news +
                ", nextNewsId=" + nextNewsId +
                ", prevNewsId=" + prevNewsId +
                ", comments=" + comments +
                '}';
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
