package com.epam.newsmanagement.service.impl;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class IntegratedServiceTest extends TestCase {

    private String fileName;

    public IntegratedServiceTest(String fileName) {
        this.fileName = fileName;
    }

    @Parameterized.Parameters
    public static Collection loadData() {
        return Arrays.asList(new Object[][]{
                {"get_client_by_id.xml"},
                {"find_clients_by_name.xml"},
                {"get_acct_list_by_unk.xml"},
                {"create_inner_document.xml"}
        });
    }

    @Test
    public void forTimeRunningTest() {
    }
}