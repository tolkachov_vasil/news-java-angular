package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.config.AppConfig;
import com.epam.newsmanagement.entity.Tag;
import org.dbunit.DataSourceBasedDBTestCase;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.dbunit.util.fileloader.DataFileLoader;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;

@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext
@ContextConfiguration(classes = {AppConfig.class})
public class TagDaoTest extends DataSourceBasedDBTestCase {

    @Autowired
    private TagDao tagDao;

    @Autowired
    private DataSource dataSource;

    @Override
    protected DataSource getDataSource() {
        return dataSource;
    }

    @Override
    protected IDataSet getDataSet() throws Exception {
        DataFileLoader loader = new FlatXmlDataFileLoader();
        return loader.load("/tag_data.xml");
    }

    @Before
    @Override
    public void setUp() throws Exception {
        DatabaseOperation.INSERT.execute(getConnection(), getDataSet());
    }

    @After
    public void after() throws Exception {
        DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSet());
    }

    @Test
    public void testFindAll() {
        Assert.assertEquals(tagDao.count(), 4);
    }

    @Test
    public void testTableNotEmpty() {
        Assert.assertTrue(tagDao.count() > 0);
    }

    @Test
    public void testInsert() {
        Tag tag = new Tag();
        tag.setTagName("testX");
        tagDao.saveAndFlush(tag);
        Assert.assertEquals(tagDao.count(), 5);
    }

    @Test
    public void testDelete() {
        tagDao.delete(1L);
        Assert.assertEquals(tagDao.count(), 3);
    }

    @Test
    public void testExists() {
        Assert.assertTrue(tagDao.exists(2L));
    }

}
