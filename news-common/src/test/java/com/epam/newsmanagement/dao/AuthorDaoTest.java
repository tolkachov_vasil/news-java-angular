package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.config.AppConfig;
import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.Author;
import org.dbunit.DataSourceBasedDBTestCase;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.dbunit.util.fileloader.DataFileLoader;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;

@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext
@ContextConfiguration(classes = {AppConfig.class})
public class AuthorDaoTest extends DataSourceBasedDBTestCase {

    @Autowired
    private AuthorDao authorDao;

    @Autowired
    private DataSource dataSource;

    @Override
    protected DataSource getDataSource() {
        return dataSource;
    }

    @Override
    protected IDataSet getDataSet() throws Exception {
        DataFileLoader loader = new FlatXmlDataFileLoader();
        return loader.load("/author_data.xml");
    }

    @Before
    @Override
    public void setUp() throws Exception {
        DatabaseOperation.INSERT.execute(getConnection(), getDataSet());
    }

    @After
    public void after() throws Exception {
        DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSet());
    }

    @Test
    public void testFindAll() {
        Assert.assertEquals(authorDao.count(), 3);
    }

    @Test
    public void testTableNotEmpty() {
        Assert.assertTrue(authorDao.count() > 0);
    }

    @Test
    public void testInsert() {
        Author author = new Author();
        author.setAuthorName("Ivan");
        authorDao.saveAndFlush(author);
        Assert.assertEquals(authorDao.count(), 4);
    }

    @Test
    public void testDelete() {
        authorDao.delete(1L);
        Assert.assertEquals(authorDao.count(), 2);
    }

    @Test
    public void testExists() {
        Assert.assertTrue(authorDao.exists(2L));
    }

}
