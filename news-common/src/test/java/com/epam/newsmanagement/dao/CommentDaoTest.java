package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.config.AppConfig;
import com.epam.newsmanagement.entity.Comment;
import org.dbunit.DataSourceBasedDBTestCase;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.dbunit.util.fileloader.DataFileLoader;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.sql.Timestamp;

@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext
@ContextConfiguration(classes = {AppConfig.class})
public class CommentDaoTest extends DataSourceBasedDBTestCase {

    @Autowired
    CommentDao commentDao;

    @Autowired
    DataSource dataSource;

    DataFileLoader loader = new FlatXmlDataFileLoader();
    IDataSet dataSet = loader.load("/comments_data.xml");

    @Override
    protected DataSource getDataSource() {
        return dataSource;
    }

    @Override
    protected IDataSet getDataSet() throws Exception {
        return dataSet;
    }

    @Before
    @Override
    public void setUp() throws Exception {
        DatabaseOperation.INSERT.execute(getConnection(), dataSet);
    }

    @After
    public void after() throws Exception {
        DatabaseOperation.DELETE_ALL.execute(getConnection(), dataSet);
    }

    @Test
    public void testFindAll() {
        Assert.assertEquals(commentDao.count(), 3);
    }

    @Test
    public void testTableNotEmpty() {
        Assert.assertTrue(commentDao.count() > 0);
    }

    @Test
    public void testInsert() {
        Comment comment = new Comment();
        comment.setNewsId(3L);
        comment.setCommentText("Text5");
        comment.setCreationDate(Timestamp.valueOf("2016-03-11 10:55:12"));
        commentDao.saveAndFlush(comment);
        Assert.assertEquals(commentDao.count(), 4);
    }

    @Test
    public void testDelete() {
        commentDao.delete(1L);
        Assert.assertEquals(commentDao.count(), 2);
    }

    @Test
    public void testExists() {
        Assert.assertTrue(commentDao.exists(2L));
    }

    @Test
    public void testCount() {
        Assert.assertEquals(commentDao.countByNewsId(1L), 1L);
        Assert.assertEquals(commentDao.countByNewsId(2L), 2L);
    }

}
