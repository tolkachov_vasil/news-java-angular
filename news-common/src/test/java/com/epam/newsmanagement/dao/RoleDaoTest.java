package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.config.AppConfig;
import com.epam.newsmanagement.dao.RoleDao;
import com.epam.newsmanagement.entity.Role;
import org.dbunit.DataSourceBasedDBTestCase;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.dbunit.util.fileloader.DataFileLoader;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;

@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext
@ContextConfiguration(classes = {AppConfig.class})
public class RoleDaoTest extends DataSourceBasedDBTestCase {

    @Autowired
    RoleDao roleDao;

    @Autowired
    DataSource dataSource;

    DataFileLoader loader = new FlatXmlDataFileLoader();
    IDataSet dataSet = loader.load("/role_data.xml");

    @Override
    protected DataSource getDataSource() {
        return dataSource;
    }

    @Override
    protected IDataSet getDataSet() throws Exception {
        return dataSet;
    }

    @Before
    @Override
    public void setUp() throws Exception {
        DatabaseOperation.INSERT.execute(getConnection(), getDataSet());
    }

    @After
    public void after() throws Exception {
        DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSet());
    }

    @Test
    public void testFindAll() {
        Assert.assertEquals(roleDao.count(), 3);
    }

    @Test
    public void testTableNotEmpty() {
        Assert.assertTrue(roleDao.count() > 0);
    }

    @Test
    public void testInsert() {
        Role role = new Role();
        role.setRoleName("NEW_ROLE");
        role.setUserId(4L);
        roleDao.saveAndFlush(role);
        Assert.assertEquals(roleDao.count(), 4);
    }

    @Test
    public void testDelete() {
        roleDao.delete(1L);
        Assert.assertEquals(roleDao.count(), 2);
    }

    @Test
    public void testExists() {
        Assert.assertTrue(roleDao.exists(2L));
    }

}
