package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.config.AppConfig;
import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.News;
import org.dbunit.DataSourceBasedDBTestCase;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.dbunit.util.fileloader.DataFileLoader;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.sql.Date;
import java.sql.Timestamp;

@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext
@ContextConfiguration(classes = {AppConfig.class})
public class NewsDaoTest extends DataSourceBasedDBTestCase {

    @Autowired
    private NewsDao newsDao;

    @Autowired
    private DataSource dataSource;

    @Override
    protected DataSource getDataSource() {
        return dataSource;
    }

    @Override
    protected IDataSet getDataSet() throws Exception {
        DataFileLoader loader = new FlatXmlDataFileLoader();
        return loader.load("/news_data.xml");
    }

    @Before
    @Override
    public void setUp() throws Exception {
        DatabaseOperation.INSERT.execute(getConnection(), getDataSet());
    }

    @After
    public void after() throws Exception {
        DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSet());
    }

    @Test
    public void testFindAll() {
        Assert.assertEquals(newsDao.count(), 3);
    }

    @Test
    public void testTableNotEmpty() {
        Assert.assertTrue(newsDao.count() > 0);
    }

    @Test
    public void testInsert() {
        News news = new News();
        news.setTitle("9Title");
        news.setShortText("9Short text");
        news.setFullText("9Full text");
        news.setCreationDate(Timestamp.valueOf("2015-09-09 10:10:30.10"));
        news.setModificationDate(Date.valueOf("2015-10-11"));
        newsDao.saveAndFlush(news);
        Assert.assertEquals(newsDao.count(), 4);
    }

    @Test
    public void testDelete() {
        newsDao.delete(1L);
        Assert.assertEquals(newsDao.count(), 2);
    }

    @Test
    public void testExists() {
        Assert.assertTrue(newsDao.exists(2L));
    }

}
