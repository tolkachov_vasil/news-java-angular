package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.config.AppConfig;
import com.epam.newsmanagement.dao.UserDao;
import com.epam.newsmanagement.entity.User;
import org.dbunit.DataSourceBasedDBTestCase;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.dbunit.util.fileloader.DataFileLoader;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;

@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext
@ContextConfiguration(classes = {AppConfig.class})
public class UserDaoTest extends DataSourceBasedDBTestCase {

    @Autowired
    UserDao userDao;

    @Autowired
    DataSource dataSource;

    DataFileLoader loader = new FlatXmlDataFileLoader();
    IDataSet dataSet = loader.load("/user_data.xml");

    @Override
    protected DataSource getDataSource() {
        return dataSource;
    }

    @Override
    protected IDataSet getDataSet() throws Exception {
        return dataSet;
    }

    @Before
    @Override
    public void setUp() throws Exception {
        // Thread.sleep(1000);
        DatabaseOperation.CLEAN_INSERT.execute(getConnection(), dataSet);
    }

    @After
    public void after() throws Exception {
        DatabaseOperation.DELETE_ALL.execute(getConnection(), dataSet);
    }

    @Test
    public void testFindAll() {
        Assert.assertEquals(userDao.count(), 3);
    }

    @Test
    public void testTableNotEmpty() {
        Assert.assertTrue(userDao.count() > 0);
    }

    @Test
    public void testInsert() {
        User user = new User();
        user.setUserName("Name");
        user.setLogin("login");
        user.setPassword("password");
        userDao.saveAndFlush(user);
        Assert.assertEquals(userDao.count(), 4);
    }

    @Test
    public void testDelete() {
        userDao.delete(1L);
        Assert.assertEquals(userDao.count(), 2);
    }

    @Test
    public void testExists() {
        Assert.assertTrue(userDao.exists(2L));
    }

}
