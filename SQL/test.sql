SELECT
  NEWS.NEWS_ID                                                       AS news_id,
  NEWS.TITLE                                                         AS title,
  (SELECT AUTHOR_NAME
   FROM AUTHOR
     JOIN NEWS_AUTHOR ON AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID
   WHERE NEWS_AUTHOR.NEWS_ID = NEWS.NEWS_ID)                         AS author,
  (SELECT COUNT(COMMENT_ID)
   FROM COMMENTS
   WHERE COMMENTS.NEWS_ID = NEWS.NEWS_ID)                            AS comments,
  (concatenate_list(CURSOR (SELECT TAG_NAME
                            FROM TAG
                              JOIN NEWS_TAG ON TAG.TAG_ID = NEWS_TAG.TAG_ID
                            WHERE NEWS.NEWS_ID = NEWS_TAG.NEWS_ID))) AS tags,
  NEWS.SHORT_TEXT                                                    AS brief,
  NEWS.MODIFICATION_DATE                                             AS news_date
FROM NEWS
WHERE NEWS_ID IN
      (SELECT NEWS_ID
       FROM
         (SELECT
            NEWS_ID,
            ROWNUM rn
          FROM
            (SELECT NEWS.NEWS_ID
             FROM NEWS
               LEFT JOIN COMMENTS ON (NEWS.NEWS_ID = COMMENTS.NEWS_ID)
               JOIN NEWS_AUTHOR ON (NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID)
               JOIN AUTHOR ON (AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID)
               JOIN NEWS_TAG ON (NEWS.NEWS_ID = NEWS_TAG.NEWS_ID)
               JOIN TAG ON (TAG.TAG_ID = NEWS_TAG.TAG_ID)
             --              WHERE
             --                NEWS_AUTHOR.AUTHOR_ID = ? AND
             --                TAG.TAG_ID IN (?)
             GROUP BY NEWS.NEWS_ID, NEWS.MODIFICATION_DATE
             ORDER BY NEWS.MODIFICATION_DATE DESC, COUNT(COMMENTS.COMMENT_ID) DESC))
       WHERE rn BETWEEN 0 AND 50);

SELECT
  NEWS.NEWS_ID                                                       AS news_id,
  NEWS.TITLE                                                         AS title,
  (SELECT AUTHOR_NAME
   FROM AUTHOR
     JOIN NEWS_AUTHOR ON AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID
   WHERE NEWS_AUTHOR.NEWS_ID = NEWS.NEWS_ID)                         AS author,
  (SELECT COUNT(COMMENT_ID)
   FROM COMMENTS
   WHERE COMMENTS.NEWS_ID = NEWS.NEWS_ID)                            AS comments,
  (concatenate_list(CURSOR (SELECT TAG_NAME
                            FROM TAG
                              JOIN NEWS_TAG ON TAG.TAG_ID = NEWS_TAG.TAG_ID
                            WHERE NEWS.NEWS_ID = NEWS_TAG.NEWS_ID))) AS tags,
  NEWS.SHORT_TEXT                                                    AS brief,
  NEWS.MODIFICATION_DATE                                             AS news_date
FROM NEWS
WHERE NEWS_ID IN (0, 1, 2, 3, 4, 5);






SELECT
  NEWS.NEWS_ID AS news_id,
  NEWS.TITLE AS title,
  (SELECT AUTHOR_NAME
   FROM
     AUTHOR
     JOIN
     NEWS_AUTHOR
       ON AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID
   WHERE
     NEWS_AUTHOR.NEWS_ID = NEWS.NEWS_ID) AS author,
  (SELECT COUNT(COMMENT_ID)
   FROM
     COMMENTS
   WHERE
     COMMENTS.NEWS_ID = NEWS.NEWS_ID) AS comments,
  (concatenate_list(CURSOR (SELECT TAG_NAME
                            FROM
                              TAG
                              JOIN
                              NEWS_TAG
                                ON TAG.TAG_ID = NEWS_TAG.TAG_ID
                            WHERE
                              NEWS.NEWS_ID = NEWS_TAG.NEWS_ID))) AS tags,
  NEWS.SHORT_TEXT AS brief,
  NEWS.MODIFICATION_DATE AS news_date
FROM
  NEWS
WHERE
  NEWS_ID IN (
    ?, ?, ?, ?, ?
  )

