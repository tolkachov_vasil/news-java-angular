-- MySQL dump 10.13  Distrib 5.7.11, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: news_management
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `AUTHOR`
--

DROP TABLE IF EXISTS `AUTHOR`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUTHOR` (
  `AUTHOR_ID`   BIGINT(20)       NOT NULL AUTO_INCREMENT,
  `AUTHOR_NAME` VARCHAR(30)
                COLLATE utf8_bin NOT NULL,
  `EXPIRED`     DATETIME(6)      NOT NULL,
  PRIMARY KEY (`AUTHOR_ID`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AUTHOR`
--

LOCK TABLES `AUTHOR` WRITE;
/*!40000 ALTER TABLE `AUTHOR` DISABLE KEYS */;
/*!40000 ALTER TABLE `AUTHOR` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `COMMENTS`
--

DROP TABLE IF EXISTS `COMMENTS`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `COMMENTS` (
  `COMMENT_ID`    BIGINT(20)       NOT NULL AUTO_INCREMENT,
  `NEWS_ID`       BIGINT(20)       NOT NULL,
  `COMMENT_TEXT`  VARCHAR(100)
                  COLLATE utf8_bin NOT NULL,
  `CREATION_DATE` DATETIME(6)      NOT NULL,
  PRIMARY KEY (`COMMENT_ID`),
  KEY `COMMENTS_FK` (`NEWS_ID`),
  CONSTRAINT `COMMENTS_FK` FOREIGN KEY (`NEWS_ID`) REFERENCES `NEWS` (`NEWS_ID`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMMENTS`
--

LOCK TABLES `COMMENTS` WRITE;
/*!40000 ALTER TABLE `COMMENTS` DISABLE KEYS */;
/*!40000 ALTER TABLE `COMMENTS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `NEWS`
--

DROP TABLE IF EXISTS `NEWS`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NEWS` (
  `NEWS_ID`           BIGINT(20)       NOT NULL AUTO_INCREMENT,
  `TITLE`             VARCHAR(30)
                      COLLATE utf8_bin NOT NULL,
  `SHORT_TEXT`        VARCHAR(100)
                      COLLATE utf8_bin NOT NULL,
  `FULL_TEXT`         VARCHAR(2000)
                      COLLATE utf8_bin NOT NULL,
  `CREATION_DATE`     DATETIME(6)      NOT NULL,
  `MODIFICATION_DATE` DATETIME         NOT NULL,
  PRIMARY KEY (`NEWS_ID`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NEWS`
--

LOCK TABLES `NEWS` WRITE;
/*!40000 ALTER TABLE `NEWS` DISABLE KEYS */;
/*!40000 ALTER TABLE `NEWS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `NEWS_AUTHOR`
--

DROP TABLE IF EXISTS `NEWS_AUTHOR`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NEWS_AUTHOR` (
  `NEWS_ID`   BIGINT(20) NOT NULL,
  `AUTHOR_ID` BIGINT(20) NOT NULL,
  KEY `NEWS_AUTHOR_FK1` (`NEWS_ID`),
  KEY `NEWS_AUTHOR_FK2` (`AUTHOR_ID`),
  CONSTRAINT `NEWS_AUTHOR_FK1` FOREIGN KEY (`NEWS_ID`) REFERENCES `NEWS` (`NEWS_ID`),
  CONSTRAINT `NEWS_AUTHOR_FK2` FOREIGN KEY (`AUTHOR_ID`) REFERENCES `AUTHOR` (`AUTHOR_ID`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NEWS_AUTHOR`
--

LOCK TABLES `NEWS_AUTHOR` WRITE;
/*!40000 ALTER TABLE `NEWS_AUTHOR` DISABLE KEYS */;
/*!40000 ALTER TABLE `NEWS_AUTHOR` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `NEWS_TAG`
--

DROP TABLE IF EXISTS `NEWS_TAG`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NEWS_TAG` (
  `NEWS_ID` BIGINT(20) NOT NULL,
  `TAG_ID`  BIGINT(20) NOT NULL,
  KEY `NEWS_TAG_FK1` (`NEWS_ID`),
  KEY `NEWS_TAG_FK2` (`TAG_ID`),
  CONSTRAINT `NEWS_TAG_FK1` FOREIGN KEY (`NEWS_ID`) REFERENCES `NEWS` (`NEWS_ID`),
  CONSTRAINT `NEWS_TAG_FK2` FOREIGN KEY (`TAG_ID`) REFERENCES `TAG` (`TAG_ID`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NEWS_TAG`
--

LOCK TABLES `NEWS_TAG` WRITE;
/*!40000 ALTER TABLE `NEWS_TAG` DISABLE KEYS */;
/*!40000 ALTER TABLE `NEWS_TAG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ROLES`
--

DROP TABLE IF EXISTS ` ROLES `;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE ` ROLES ` (
  `USER_ID`   BIGINT(20)       NOT NULL,
  `ROLE_NAME` VARCHAR(50)
              COLLATE utf8_bin NOT NULL,
  KEY `ROLES_FK` (`USER_ID`),
  CONSTRAINT `ROLES_FK` FOREIGN KEY (`USER_ID`) REFERENCES ` USER ` (`USER_ID`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ROLES`
--

LOCK TABLES ` ROLES ` WRITE;
/*!40000 ALTER TABLE `ROLES` DISABLE KEYS */;
/*!40000 ALTER TABLE `ROLES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TAG`
--

DROP TABLE IF EXISTS `TAG`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TAG` (
  `TAG_ID`   BIGINT(20)       NOT NULL AUTO_INCREMENT,
  `TAG_NAME` VARCHAR(30)
             COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`TAG_ID`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TAG`
--

LOCK TABLES `TAG` WRITE;
/*!40000 ALTER TABLE `TAG` DISABLE KEYS */;
/*!40000 ALTER TABLE `TAG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USERS`
--

DROP TABLE IF EXISTS `USERS`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `USERS` (
  `USER_ID`    BIGINT(20)       NOT NULL AUTO_INCREMENT,
  `USER_NAME`  VARCHAR(50)
               COLLATE utf8_bin NOT NULL,
  `LOGIN`      VARCHAR(30)
               COLLATE utf8_bin NOT NULL,
  ` PASSWORD ` VARCHAR(30)
               COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`USER_ID`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USERS`
--

LOCK TABLES `USERS` WRITE;
/*!40000 ALTER TABLE `USERS` DISABLE KEYS */;
/*!40000 ALTER TABLE `USERS` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2016-05-22 12:11:58