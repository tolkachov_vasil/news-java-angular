INSERT INTO NEWS_PROD.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (1, 'Chinese medicine plant secrets probed
', 'Scientists have unravelled one of the secrets of a plant used in traditional Chinese medicine.
', 'The Chinese skullcap - known as Huang-Qin - is traditionally used for fever, liver and lung problems.
Scientists have discovered that the plant uses a special pathway to make chemicals with potential cancer-fighting properties.
They say it is a step towards being able to scale up production to make new drugs.
Prof Cathie Martin, of the John Innes Centre in Norwich, is lead researcher of the study, published in Science Advances.
Working in collaboration with Chinese scientists, her team deduced how the plant, Scutellaria baicalensi, synthesises the chemicals, known as flavones.
Flavones are found widely in the plant kingdom, giving some plants vivid blue flowers.
"Understanding the pathway should help us to produce these special flavones in large quantities, which will enable further research into their potential medicinal uses," said Prof Martin.
"It''s exciting to consider that the plants which have been used as traditional Chinese remedies for thousands of years may lead to effective modern medicines."
Ancient remedy', TO_TIMESTAMP('2015-11-12 09:37:39.923000', 'YYYY-MM-DD HH24:MI:SS.FF6'), TO_DATE('2015-11-12 09:37:43', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO NEWS_PROD.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (2, 'Are these Britain''s youngest Antarctic researchers?
', 'British scientists who have set up a network of penguin-monitoring cameras throughout Antarctica are asking the public to help them carry out their research.
', 'British scientists who have set up a network of penguin-monitoring cameras throughout Antarctica are asking the public to help them carry out their research.
The Oxford team is launching a new version of their ambitious citizen science project, PenguinWatch, on Thursday 7 April - enabling people to explore Antarctica''s penguin colonies online via images captured by the remote cameras.
The new site will allow young enthusiasts, like these children at a primary school in Cheshire, to monitor the results of their own research, and even to adopt and monitor their own penguin colony.
More on this story on the BBC News at Six and the full report on Our World: The Penguin Watchers, Saturday and Sunday at 2130 on the BBC News Channel
', TO_TIMESTAMP('2015-11-12 09:37:39.923000', 'YYYY-MM-DD HH24:MI:SS.FF6'), TO_DATE('2015-11-12 09:37:43', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO NEWS_PROD.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (3, 'McCarthy dethrones Batman v Superman at US box office
', 'Melissa McCarthy''s new comedy The Boss has unseated Batman v Superman: Dawn Of Justice from the top of the North American and Canadian box office.
', 'The film, in which McCarthy plays an eccentric tycoon, took an estimated $23.5m (£16.5m) on its debut weekend.
Dawn of Justice was knocked to second place in its third week, pulling in an estimated $23.4m (£16.4m).
But when final box office figures are released on Monday, the DC comics "mash-up" could still come out on top.
The margin between the top two films is one of the closest in recent years, according to industry analysts.
Ben Affleck and Henry Cavill''s superhero movie has defied poor reviews, having taken almost $300m (£211m) in the US.
The film, in which Affleck plays Batman and Cavill takes the role of Superman, now ranks as the third highest-grossing DC Comics release to date in the US, according to Box Office Mojo.
Its box office haul has now surpassed 2013''s Man of Steel, also starring Cavill, which took $291m (£205m). However, Dawn of Justice''s takings has dropped by 54% since its first week of release.
The Boss was co-written by Bridesmaids star McCarthy and her husband Ben Falcone.
Critics had not expected it to perform well, but the film has done better than McCarthy''s 2014 comedy Tammy.
In third place came Disney''s Zootopia with $14.4m (£10m), slipping one spot from last week.
', TO_TIMESTAMP('2015-11-12 09:37:39.923000', 'YYYY-MM-DD HH24:MI:SS.FF6'), TO_DATE('2015-11-12 09:37:43', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO NEWS_PROD.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (4, 'Vitamin D ''heals damaged hearts''
', 'Vitamin D supplements may help people with diseased hearts, a study suggests.
', 'A trial on 163 heart failure patients found supplements of the vitamin, which is made in the skin when exposed to sunlight, improved their hearts'' ability to pump blood around the body.
The Leeds Teaching Hospitals team, who presented at a meeting of the American College of Cardiology, described the results as "stunning".
The British Heart Foundation called for longer trials to assess the pills.
Vitamin D is vital for healthy bones and teeth and may have important health benefits throughout the body but many people are deficient.
No safe way to suntan - warning
The average age of people in the study was 70 and, like many people that age, they had low levels of vitamin D even in summer.
"They do spend less time outside, but the skin''s ability to manufacture vitamin D also gets less effective [with age] and we don''t really understand why that is," said consultant cardiologist Dr Klaus Witte.
Patients were given either a 100 microgram vitamin D tablet or a sugar pill placebo each day for a year.
And researchers measured the impact on heart failure - a condition in which the heart becomes too weak to pump blood properly.
The key measure was the ejection fraction, the amount of blood pumped out of the chambers of the heart with each beat.
In a healthy adult, the figure is between 60% and 70%, but only a quarter of the blood in the heart was being successfully pumped out in the heart failure patients.
But in those taking the vitamin pills, the ejection fraction increased from 26% to 34%.
Dr Witte told the BBC News website: "It''s quite a big deal, that''s as big as you''d expect from other more expensive treatments that we use, it''s a stunning effect.
"It''s as cheap as chips, has no side effects and a stunning improvement on people already on optimal medical therapy, it is the first time anyone has shown something like this in the last 15 years."
The study also showed the patients'' hearts became smaller - a suggestion they are becoming more powerful and efficient.
', TO_TIMESTAMP('2015-11-12 09:37:39.923000', 'YYYY-MM-DD HH24:MI:SS.FF6'), TO_DATE('2015-11-12 09:37:43', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO NEWS_PROD.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (5, 'Daily Mail owner considering Yahoo bid
', 'The owner of the Daily Mail newspaper is in talks with other parties about a bid for the struggling US internet company Yahoo.
', 'A spokesman said discussions were "at a very early stage" and there was no certainty a deal would take place.
The Wall Street Journal, where it was first reported, said the Daily Mail and General Trust (DMGT) was discussing an offer with private equity firms.
Yahoo is under pressure from shareholders to turn itself around.
The activist hedge fund investor Starboard Value recently called for the replacement of the entire board at the loss-making company.
The spokesman for Daily Mail said: "Given the success of DailyMail.com and Elite Daily we have been in discussions with a number of parties who are potential bidders.
"Discussions are at a very early stage and there is no certainty that any transaction will take place."
Deadline
DMGT shares were flat in early trading at 695p after initially falling 0.4%. The company is valued at £2.34bn.
The Wall Street Journal, citing people familiar with the matter, said that the potential bid could take two forms.
In one scenario, a private-equity partner would acquire Yahoo''s core web business with the Mail taking over the news and media properties.
In another scenario, the private-equity firm would acquire Yahoo''s core web business and merge its media and news properties with the Mail''s online operations.
Yahoo''s media assets include Yahoo News, Yahoo Finance, Yahoo Sports and a range of digital magazines.
The US company has set a deadline for 18 April for interested parties to submit their offers.
Time Inc is also reported to be weighing a bid together with a private equity firm.
', TO_TIMESTAMP('2015-11-12 09:37:39.923000', 'YYYY-MM-DD HH24:MI:SS.FF6'), TO_DATE('2015-11-12 09:37:43', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO NEWS_PROD.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (6, 'John Kerry makes historic visit to Hiroshima memorial
', 'The US Secretary of State said his visit to the Japanese city of Hiroshima was a "gut-wrenching" reminder of the need to get rid of nuclear weapons.
', 'John Kerry made the comments at a press conference after laying a wreath at the city''s atomic bomb memorial.
He is the first US secretary of state ever to visit the memorial or the city.
Around 140,000 people, most of them civilians, were killed when the US dropped its atomic bomb on the city in 1945.
Describing it as "a display that I will, personally, never forget" he said: "It reminds everybody of the extraordinary complexity of choices in war and of what war does to people, to communities, to countries, to the world."

Mr Kerry was joined by foreign ministers from the G7 group of nations who are holding talks in the city. They laid wreaths at the memorial and observed a minute of silence.
As well as the Hiroshima Peace Park memorial, the ministers also visited the Bomb Dome, over which the A-bomb exploded, and the nearby Hiroshima museum, which tells the personal stories of people who died.', TO_TIMESTAMP('2015-11-12 09:37:39.923000', 'YYYY-MM-DD HH24:MI:SS.FF6'), TO_DATE('2015-11-12 09:37:43', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO NEWS_PROD.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (7, 'N Korea senior intelligence officer ''defects to South''
', 'A senior North Korean military officer who oversaw spying operations has defected, say South Korean officials.
', 'The officer has not been named, but the defence ministry in Seoul said he was a senior colonel in the Reconnaissance General Bureau and left last year.
South Korea''s Yonhap news agency quoted a source as saying the colonel was seen as elite by other defectors.
More than 28,000 people have fled North Korea since the end of the Korean War, but high level defections are rare.
Last week, 13 North Koreans who had been working in one of the North''s restaurants abroad defected as a group.
Yonhap said a number of senior political figures had defected while working overseas recently.
It quoted government officials as saying this was a sign the leadership of Kim Jong-un was cracking.
''Valuable information''
Defence Ministry spokesman Moon Sang-gyun said the South could not release further information on the colonel.

One unnamed official told Yonhap the man was the highest-level military official ever to have defected.
"He is believed to have stated details about the bureau''s operations against South Korea to the authorities here," said the official.
The Reconnaissance General Bureau handles intelligence gathering and spying operations, as well as cyber warfare, said Yonhap.
The BBC''s Stephen Evans in Seoul said such a figure would likely have valuable information about the workings of Kim Jong-un''s government.', TO_TIMESTAMP('2015-11-12 09:37:39.923000', 'YYYY-MM-DD HH24:MI:SS.FF6'), TO_DATE('2015-11-12 09:37:43', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO NEWS_PROD.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (8, 'EU to make big firms come clean on tax
', 'Plans to force the largest companies to disclose more about their tax affairs will be unveiled by the European Union on Tuesday.
', 'Britain''s EU Commissioner, Lord Hill, is set to present the rules, which will affect multinational firms with more than €750m (£600m) in sales.
They will have to detail how much tax they pay and in which EU countries.
The plans come amid heightened scrutiny of the use of tax havens following the Panama Papers revelations.
Transparency
Lord Hill, the EU''s financial services commissioner, said: "This is a carefully thought through but ambitious proposal for more transparency on tax.
"While our proposal on [country-by-country reporting] is not of course focused principally on the response to the Panama Papers, there is an important connection between our continuing work on tax transparency and tax havens that we are building into the proposal."
Country-by-country reporting rules already apply to banks, mining and forestry companies, according to an EU spokesperson.
Under the new proposals, that would be expanded to cover companies accounting for about 90% of corporate revenues in the EU, they added.
The BBC understands that companies will need to disclose information such as total net turnover, profit before tax, income tax due, amount of tax actually paid and accumulated earnings.
The changes come after G20 leaders agreed to follow an OECD action plan to tackle corporate tax minimisation.', TO_TIMESTAMP('2015-11-12 09:37:39.923000', 'YYYY-MM-DD HH24:MI:SS.FF6'), TO_DATE('2015-11-12 09:37:43', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO NEWS_PROD.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (9, 'Martha Lane Fox to join Twitter board
', 'Digital pioneer Martha Lane Fox has tweeted that she will be joining the board of US-based social network Twitter.
', 'Baroness Lane-Fox tweeted that it would be the "best job ever".
Twitter has suffered some poor financial results lately, with a quarterly net loss of $90m (£64m) reported in February.
Pepsi chief financial officer Hugh Johnston will also join the social network''s board.
Baroness Lane-Fox is widely regarded as one of Britain''s most successful web pioneers.
She was the UK''s Digital Champion until 2013, the same year she joined the House of Lords
She began her digital career with the launch of lastminute.com along with co-founder Brent Hoberman in 1998.
By the time they sold it in 2005, the deal valued the company at £577m.
Recently, she founded Dot Everyone, a body that champions digital innovation.
''The Brits are coming''
"I''m absolutely over the moon to be part of the journey of an iconic company that I love using," Baroness Lane-Fox told the BBC.
"Watch out Silicon Valley - the Brits are coming," she added.
It was also announced that Pepsi vice chairman and chief financial officer Hugh Johnston would join Twitter''s board.
Twitter executive chairman Omid Kordestani tweeted that he was "thrilled" by the news.
"It''s a really interesting appointment," Richard Holway, chairman of the TechMarketView consultancy, told the BBC.
But he added that in his opinion Twitter faced an uphill struggle when it came to turning the company''s fortunes around.
"I think Twitter has huge problems and it will need some pretty major changes in order to make it relevant and, in particular, profitable."', TO_TIMESTAMP('2015-11-12 09:37:39.923000', 'YYYY-MM-DD HH24:MI:SS.FF6'), TO_DATE('2015-11-12 09:37:43', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO NEWS_PROD.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (10, 'Major Tim Peake ''catches the Dragon''
', 'UK astronaut Tim Peake has successfully berthed the latest cargo ship to visit the International Space Station.
', 'Using the platform''s robotic arm, the Briton reached out to grab the Dragon freighter on Sunday, pulling it into the Harmony module.
"We show load is safe, and it looks like we''ve caught a Dragon," said Major Tim.
The capsule, sent up by private contractor SpaceX, is carrying just over three tonnes of equipment.
Included in the shipment is an expandable room that will be fitted to the ISS in the coming weeks.
Also in the cargo are Chinese cabbage seeds that astronauts will attempt to grow in orbit, and a group of mice which will be used to test drugs that might help combat muscle and bone loss in the weightless environment of space.
Meanwhile back on Earth, the main stage of the Falcon rocket that sent the Dragon on its way to the station on Friday has returned to port in Florida.
The booster made a return to a drone ship stationed out in the Atlantic after completing its ascent - the first time in five attempts that SpaceX had managed to recover one of its vehicles at sea.', TO_TIMESTAMP('2015-11-12 09:37:39.923000', 'YYYY-MM-DD HH24:MI:SS.FF6'), TO_DATE('2015-11-12 09:37:43', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO NEWS_PROD.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (11, 'Peru election: Keiko Fujimori wins first round, early results say
', 'Centre-right candidate Keiko Fujimori has won the first round of Peru''s presidential election, early results say.
', 'With two-fifths of ballots counted, Ms Fujimori had 39% and appeared likely to face Pedro Kuczynski, a former World Bank economist, in a June run-off vote.
Mr Kuczynski had 24% while leftist Veronika Mendoza had 17%.
Ms Fujimori, the daughter of former President Alberto Fujimori, says tackling crime is her priority.
Shadow of jailed ex-president cast over Peru polls
She is also supported by some Peruvians who credit her father with defeating the country''s Maoist Shining Path rebel group.
However, other Peruvians have said they would never support anyone associated with her father, who is currently serving 25 years in prison for ordering death squads to massacre civilians during his attempts to end the insurgency.
The Shining Path rebel group was largely dismantled in the 1990s after a decade-long conflict that killed about 69,000 people.
However, rebels estimated to number in the hundreds still control areas of jungle in a coca-growing region of the country and the Peruvian authorities say they have joined forces with drug gangs.
Remnants of the group are thought to have been behind a deadly attack on a vehicle carrying election materials in a remote coca-growing region ahead of the election.
Peru is one of the biggest coca leaf and cocaine producers in the world, according to the US authorities.
', TO_TIMESTAMP('2015-11-12 09:37:39.923000', 'YYYY-MM-DD HH24:MI:SS.FF6'), TO_DATE('2015-11-12 09:37:43', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO NEWS_PROD.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (12, 'Japan''s Nikkei falls as yen strengthens
', 'Japan''s Nikkei share index fell, with shares in exporters hit as the yen continued to strengthen.
', 'At the close, Japan''s benchmark Nikkei 225 index had fallen 0.4% to 15,607.77.
The continued strength in the yen dragged down shares of export-related companies, including car manufacturers Toyota, Honda and Nissan.
The dollar fell below 108 yen on Monday, and is now back to values last seen in October 2014.
Separately, data from Japan showed that core machinery orders fell 9.2% in February compared with the previous month.
That was being seen as a sign that business investment remains subdued for companies operating in Japan.

WiseTech market debut
In China, the Shanghai Composite index rose 1.6% to close at 3,033.96, while in Hong Kong the Hang Seng index gained 0.35% to end at 20,440.81 points.
In Australia, the benchmark S&P ASX 200 closed down 0.1% at 5,012.00. WiseTech Global was a standout performer as it made its debut on the exchange, with shares of the logistics software company rising 13%.
The listing raised 170m Australian dollars ($128m; £90.6m) for WiseTech. The Australian firm recently won a major contract with international logistics giant DHL, while at home its clients include the state-owned Australia Post.
In South Korea, the benchmark Kospi closed down 0.1% at 1,970.37.
This week''s calendar
As for things to look out for this week - China will release first quarter growth numbers on Friday. That will be the most-watched gauge on how the world''s second largest economy is performing against a backdrop of a global slowdown.
Analysts are forecasting an expansion of 6.8% when compared with the same period last year.
On Wall Street, the first quarter earnings reporting season for corporate America kicks off later today, with Alcoa being the first to announce its results.
US banks are due to report their quarterly earnings later in the week.', TO_TIMESTAMP('2015-11-12 09:37:39.923000', 'YYYY-MM-DD HH24:MI:SS.FF6'), TO_DATE('2015-11-12 09:37:43', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO NEWS_PROD.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (13, 'Rogue One: Star Wars spin-off trailer released', 'The first trailer for the new Star Wars movie Rogue One has been released.
', 'The first trailer for the new Star Wars movie Rogue One has been released.
The one minute, 38 second teaser reveals new-look stormtroopers and details about the lead character played by British actress Felicity Jones.
The film, out in December, is the first in a series of on-screen adventures exploring stories outside the core Star Wars saga.
Courtesy Walt Disney Pictures/Lucasfilm
', TO_TIMESTAMP('2015-11-12 09:37:39.923000', 'YYYY-MM-DD HH24:MI:SS.FF6'), TO_DATE('2015-11-12 09:37:43', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO NEWS_PROD.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (14, 'President Obama: Libya aftermath ''worst mistake'' of presidency
', 'US President Barack Obama has said failing to prepare for the aftermath of the ousting of Libyan leader Col Muammar Gaddafi was the worst mistake of his presidency.', 'Mr Obama was answering a series of questions on the highs and lows of his time in office on Fox News.
He said, however, that intervening in Libya had been "the right thing to do".
The US and other countries carried out strikes designed to protect civilians during the 2011 uprising.
But after the former Libyan leader was killed, Libya plunged into chaos with militias taking over and two rival parliaments and governments forming.
So-called Islamic State (IS) gained a foothold, and Libya became a major departure point for migrants trying to reach Europe.
A UN-backed national unity government arrived in the capital Tripoli earlier this month but is waiting to take charge.
The leader of the faction ruling western Libya has threatened to prosecute any of his ministers who co-operate with the UN-backed administration, contradicting an earlier announcement the ministers would stand down.
President Obama gave the brief but revealing answer speaking to Chris Wallace:
CW: Worst mistake?
Obama: Probably failing to plan for the day after, what I think was the right thing to do, in intervening in Libya.
', TO_TIMESTAMP('2015-11-12 09:37:39.923000', 'YYYY-MM-DD HH24:MI:SS.FF6'), TO_DATE('2015-11-12 09:37:43', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO NEWS_PROD.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (15, 'China aims to become football superpower ''by 2050''
', 'China has unveiled a strategy to become a "world football superpower" by 2050, with plans to get 50 million children and adults playing the game by 2020.
', 'Other targets include providing at least 20,000 football training centres and 70,000 football pitches by 2020.
While China excels at the Olympics and Paralympics, it has only ever qualified for one football World Cup, in 2002.
President Xi Jinping is a football enthusiast and previously said he wants China to win the World Cup in 15 years.
The plan was published by the Chinese Football Association (in Chinese) on Monday.
It sets out short, medium and long-term targets, including ensuring there is one football pitch for every 10,000 people by 2030.
By 2050, the report added, China should be "a first-class football superpower" that "contributes to the international football world".
The men''s football team should become one of the best in Asia, while the female football team should be ranked as a world-class team, by 2030.
China''s men''s team currently sits 81st in the Fifa world rankings, out of 204 nations, below far smaller nations such as Haiti, Panama and Benin.
The football league has been hit by corruption in recent years, with 33 players and officials banned in 2013 for match-fixing, although efforts have been made to clean up the sport.', TO_TIMESTAMP('2015-11-12 09:37:39.923000', 'YYYY-MM-DD HH24:MI:SS.FF6'), TO_DATE('2015-11-12 09:37:43', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO NEWS_PROD.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (16, 'Puttingal temple: Five detained over India fireworks blast
', 'Police in India have detained five people in connection with an explosion and fire at a Hindu temple in Kerala that killed more than 100 people.
', 'The five men who work at the Puttingal temple in Paravur, are being questioned about an unauthorised fireworks display that sparked the massive explosion.
Authorities have already ordered a judicial inquiry into the incident.
Police are also looking for a number of people, who were in charge of running the temple and the festival.
Nearly 400 others were injured near the temple when a faulty rocket fell onto a large stockpile of fireworks.
A building at the temple then collapsed, causing many of the deaths.
Thousands had gathered to watch the display as part of a local new year festival.
Officials say the temple had been denied permission on safety grounds, but it had gone ahead under pressure from a large crowd.
"Five workers have been detained for questioning," Kollam police chief P Prakash told AFP news agency.
"These are not formal arrests. Once they are questioned, only then we will know their involvement and take further steps."
Indian Prime Minister Narendra Modi, who visited the scene and spoke with victims in local hospitals, said the incident was "heart-rending and shocking beyond words".
"The incident is so grave that it is very difficult to describe in words," he added. "People who were 200 metres away were also hurt."', TO_TIMESTAMP('2015-11-12 09:37:39.923000', 'YYYY-MM-DD HH24:MI:SS.FF6'), TO_DATE('2015-11-12 09:37:43', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO NEWS_PROD.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (17, 'Russian S-300 air defence missiles ''arrive in Iran''
', 'Russia is reported to have started delivering S-300 surface-to-air missiles to Iran, under a deal opposed by Israel, the US and Saudi Arabia.
', 'Iran''s foreign ministry spokesman Hossein Jaberi-Ansari said "the first stage of the contract has been implemented".
It is not yet clear how many missiles may have been delivered.
The controversial contract got the go-ahead after international sanctions on Iran were lifted last year.
The diplomatic breakthrough involved a deal over Iran''s atomic programme, imposing new international safeguards aimed at preventing Iran from developing nuclear weapons.
Iran has insisted its nuclear programme is for purely peaceful purposes and denies seeking to build a nuclear bomb.
Profile: Russia''s S-300 missile system
The $800m (£562m) contract, signed in 2007, was frozen by Russia in 2010 because of the international sanctions. President Vladimir Putin unfroze it a year ago.
Israel and the US fear the missiles could be used to protect Iranian nuclear sites from air strikes.
The S-300, made by Rostec, can be used against multiple targets including jets, or to shoot down other missiles.
The S-300V4 variant, delivered to the Russian armed forces in 2014, can shoot down any medium-range missile in the world today, flies at five times the speed of sound and has a range of 400km (249 miles), Russia''s Tass news agency reports.
Rostec chief Sergei Chemezov had told the Wall Street Journal last month that delivery of the system to Iran was expected at the end of this year.', TO_TIMESTAMP('2015-11-12 09:37:39.923000', 'YYYY-MM-DD HH24:MI:SS.FF6'), TO_DATE('2015-11-12 09:37:43', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO NEWS_PROD.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (18, 'Luxury boat builders target Asia''s rich
', 'Sailing the high seas on a multi million dollar yacht is a luxury not everyone can afford', 'Sailing the high seas on a multi million dollar yacht is a luxury not everyone can afford.
That is why an increasing number of boat-makers are setting their sights on the ultra-wealthy in Asia.
But are they buying?
The BBC''s Leisha Chi reports from the Singapore Yacht Show.', TO_TIMESTAMP('2015-11-12 09:37:39.923000', 'YYYY-MM-DD HH24:MI:SS.FF6'), TO_DATE('2015-11-12 09:37:43', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO NEWS_PROD.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (19, 'Tata announces Scunthorpe plant deal
', 'Tata Steel has confirmed a deal to sell its Long Products Europe business, including its Scunthorpe plant, to UK-based investment firm Greybull Capital.
', 'The move will safeguard more than 4,000 jobs, but workers are being asked to accept a pay cut and less generous pension arrangements.
Greybull said it was arranging a £400m investment package as part of the deal.
The business will be rebranded as "British Steel" once the deal is completed in eight weeks, it said.
The new business would include the Scunthorpe works, two mills in Teesside, an engineering workshop in Workington, a design consultancy in York, a mill in Hayange, France, and sales and distribution facilities.
Greybull partner Marc Meyohas said he was "delighted" with the agreement and that he believed the division could become a "strong business".
"We are now focused on taking the deal to completion in order that the business can start its next chapter with confidence," he added.
Turnaround plan
The Long Products Europe business makes steel for the rail and construction sectors.
The division was put up for sale in 2014. Greybull, whose interest was widely known, has been in talks with Tata for the past nine months over a possible deal.
Greybull is backing a turnaround plan, which aims to return the loss-making business to profitability within one to two years, but will involve significant cost savings.
Staff are being asked to accept a 3% pay cut for one year and reductions to company pension contributions. A staff ballot on the changes will be completed on 19 April.
But Greybull said its plan "to reset the cost base of the business" had already been agreed with both trade unions and key suppliers.
The turnaround plan was drawn up by management at Scunthorpe with input from unions and refined by management consultancy firm McKinsey & Co.
It is understood Greybull is not envisaging further restructuring beyond the 1,200 job losses announced last October.
', TO_TIMESTAMP('2015-11-12 09:37:39.923000', 'YYYY-MM-DD HH24:MI:SS.FF6'), TO_DATE('2015-11-12 09:37:43', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO NEWS_PROD.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (20, 'Panama papers: Mossack Fonseca offices in El Salvador raided
', 'Authorities in El Salvador have raided the offices of the Panama law firm at the centre of a massive data leak, the attorney general''s office says.
', 'Documents and computer equipment were seized from the Mossack Fonseca office, officials said on Twitter.
The attorney general''s office said the Mossack Fonseca sign had been removed a day earlier and quoted an employee as saying the firm was moving.
The leak showed how some wealthy people use offshore companies to evade tax.
The raid was overseen by El Salvador''s Attorney General Douglas Melendez.
', TO_TIMESTAMP('2015-11-12 09:37:39.923000', 'YYYY-MM-DD HH24:MI:SS.FF6'), TO_DATE('2015-11-12 09:37:43', 'YYYY-MM-DD HH24:MI:SS'));